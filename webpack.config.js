var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /(\.css)$/,
          use:['style-loader','css-loader']
        },
        {
          test: /(\.scss)$/,
          use:['style-loader','css-loader','sass-loader']
        },
        {
          test: /\.mtl$/,
          loader: 'mtl-loader'
        },
        { 
          test: /\.obj$/,
          loader: 'url-loader',
        },
        {
          test: /\.(png|gif|jpg|svg)$/,
          use: [{
            loader: 'file-loader',
          }]
        },
      ]
    }
  };