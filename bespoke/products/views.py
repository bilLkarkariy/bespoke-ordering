from products.models import Product, Option, Item
from products.serializers import ProductSerializer, OptionSerializer, ItemSerializer
from rest_framework import generics, pagination
from django.core.paginator import Paginator
from rest_framework.pagination import CursorPagination

class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 1000000000
    page_size_query_param = 'page_size'
    max_page_size = 10000

class StandardResultsSetPagination(pagination.PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'
    max_page_size = 10000
    def setPageSize(self,page_size):
        self.page_size = page_size
    
class ProductListView(generics.ListAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class ProductDetailView(generics.RetrieveUpdateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class OptionListView(generics.ListAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
    
class OptionDetailView(generics.RetrieveUpdateAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
       
class ItemListView(generics.ListAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

class ItemDetailView(generics.RetrieveUpdateAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    
    
class OptionsOfProductListView(generics.ListAPIView):
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
    
    def get_queryset(self):
        return Option.objects.filter(products_owner=self.kwargs['pk'])   
    
    
    
class ItemsOfOptionsOfProductListView(generics.ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, '_paginator'):
            # import ipdb
            # ipdb.set_trace()
            page_size = self.request.GET.get('page_size')
            if page_size == None or int(page_size) == 0 :
                self._paginator = LargeResultsSetPagination()
            else:
                print(page_size)
                self._paginator.setPageSize(page_size)
                self._paginator = StandardResultsSetPagination()
        return self._paginator

    def get_queryset(self):
        list_item = Item.objects.filter(option_link=self.kwargs['pk'])
            
        return list_item
    
    