TYPES_OPTIONS = (
	('', u"Type"),
	('a', u"applicatif"),
	('c', u"color"),
	('i', u"initiale"),
	('s', u"selection")
)

QUALITY_OPTIONS = (
	('', u"Quality"),
	('c', u"Classic"),
	('s', u"Signature"),
	('l', u"Legacy"),
	('p', u"Premium"),
	('a', u"Autre Marque"),
	('co', u"Confection")
)
