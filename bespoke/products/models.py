from django.db import models
from products import TYPES_OPTIONS, QUALITY_OPTIONS
from djmoney.models.fields import MoneyField
import cv2
from PIL import Image, ImageEnhance
import numpy as np
from bespoke import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
import io
# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=33, blank=False, null=False)
    # image of the item
    default_texture = models.ImageField(upload_to=__name__)
    class Meta:
        ordering = ['id']
    def __str__(self):
        return self.name
    
class Option(models.Model):
    name = models.CharField(max_length=40, blank=False, null=False)

    # an option belongs to one or several Products
    products_owner = models.ManyToManyField(Product, blank=False)
    # is the option active, if no the option is not displayed 
    is_active = models.BooleanField(default=True)

    # is this option is facultative ?
    isFacultatif = models.BooleanField(default=False, null=True)

    # can the option be additive with an another one
    is_unique = models.BooleanField(default=True, null=True)

    # type of the option
    type = models.CharField(max_length=1,default='a', choices=TYPES_OPTIONS)
   
    ref = models.CharField(max_length=40,blank=True, null=True)
   


    class Meta:
        ordering = ['id']
    def __str__(self):
        return self.name
    
    
class Item(models.Model):
    # name of the item
    name = models.CharField(max_length=40, blank=False, null=False)

    # reference of the item
    ref = models.CharField(max_length=66, blank=False, null=False)
     
    # description of the Item
    description = models.CharField(max_length=100, blank=True, null=True)
   
    # price of the Item
    price = MoneyField(max_digits=6, decimal_places=2,  blank=True, null=True, default_currency='EUR')
   
    # this Item belongs to an option
    option_link = models.ForeignKey(Option, on_delete=models.PROTECT)
   
    # image of the item
    image = models.ImageField(upload_to=__name__)

    
    # image of the item
    texture = models.ImageField(upload_to='textures')

    class Meta:
        ordering = ['id']
    def __str__(self):
        return self.name

