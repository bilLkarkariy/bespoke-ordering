from django.contrib import admin
from django.urls import path, include
from django.conf.urls import  url
from rest_framework import routers
from products.views import ProductListView, OptionListView, ItemListView, ProductDetailView, OptionDetailView, ItemDetailView, OptionsOfProductListView, ItemsOfOptionsOfProductListView
from rest_framework import generics
from products.models import Product
from products.serializers import ProductSerializer

urlpatterns = [
    url(r'^api-bespoke/products/$', ProductListView.as_view(), name='products-list'),
    url(r'^api-bespoke/products/(?P<pk>[0-9]+)/$', ProductDetailView.as_view(),name='products-detail'),
    url(r'^api-bespoke/options/$', OptionListView.as_view(), name='options-list'),
    url(r'^api-bespoke/options/(?P<pk>[0-9]+)/$', OptionDetailView.as_view(),name='options-detail'),
    url(r'^api-bespoke/items/$', ItemListView.as_view(), name='items-list'),
    url(r'^api-bespoke/items/(?P<pk>[0-9]+)/$', ItemDetailView.as_view(),name='items-detail'),
    
        url(r'^api-bespoke/products/(?P<pk>[0-9]+)/options/$', OptionsOfProductListView.as_view(),name='options-product-list'),
        url(r'^api-bespoke/options/(?P<pk>[0-9]+)/items/$', ItemsOfOptionsOfProductListView.as_view(),name='items-options-product-list'),

]