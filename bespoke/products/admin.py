from django.contrib import admin
from .models import Product, Option, Item
from django.utils.html import format_html

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id','name',)

admin.site.register(Product, ProductAdmin)

class OptionAdmin(admin.ModelAdmin):
    list_display = ('id','name','_get_products','isFacultatif','is_unique','type',)
    def _get_products(self, obj):
        return obj.products_owner
    
    _get_products.short_description = "Produit(s) associé(s)"

admin.site.register(Option, OptionAdmin)

class ItemAdmin(admin.ModelAdmin):
    list_display = ('id','name','_option_link','_products_owner','price','image_tag')

    def _products_owner(self, obj):
        return obj.option_link.products_owner
    def _option_link(self, obj):
        return obj.option_link
    def image_tag(self, obj):
        return format_html('<img src="{}" height=200 />'.format(obj.image.url))

    image_tag.short_description = 'Image'

    _products_owner.short_description = "Produit associé"
    _option_link.short_description   = "Option associé"

admin.site.register(Item, ItemAdmin)