from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager, UserManager
import datetime
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractBaseUser):
       nom = models.CharField(max_length=100)
       prenom = models.CharField(max_length=100) 
       email = models.EmailField(max_length=100, unique=True)
       telephone = PhoneNumberField()
       created_at = models.DateTimeField(auto_now_add=True)
       updated_at = models.DateTimeField(auto_now=True)

       REQUIRED_FIELDS = [ 'email','prenom', 'nom', 'telephone' ]
       
       objects = UserManager()

       def __str__(self):            
              return self.email

       def get_full_name(self):
              return (self.prenom+' '+self.nom)
 
       def get_short_name(self):
              return self.prenom
 
       