
import {Button, Card}  from 'react-bootstrap'
import {Row, Col}  from 'react-bootstrap'
import React from "react";
import ReactCardFlip from 'react-card-flip';

/**
 * 
 */
class BodyCard extends React.Component {
       /**
        * @param isFlipped : state of the card, is the card is flipped or not
        * @param {*} props 
        */
       constructor(props) {
              super(props);
              this.state = {
                     isFlipped: false,
              };           
              this.handleClick = this.handleClick.bind(this);
              this.addCart = this.addCart.bind(this);
       }
       handleClick(e) {
              e.preventDefault();
              this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
       }
       addCart(e){
              this.props.props_cart(this.props.item);            
       }
       
       render() {
              const {item} = this.props;
              const back = (item.description == null ? '' : 
              <Card key="back">
                                          <Card.Body>
                                                 <Card.Title className="responsive-txt">{item.name}</Card.Title>
                                                        <Card.Text>
                                                               {item.description}
                                                        </Card.Text>
                                                 <Row>
                                                        <div className="container">
                                                               <Button onClick={this.handleClick} className="col-3 text-left" variant="light"  size="sm">
                                                                      <i className="fas fa-undo" style={{fontWeight: 550}}></i>
                                                               </Button>
                                                               <Button onClick={this.addCart} className="col-4 offset-5" variant="outline-primary" size="sm">
                                                                      <i className="fas fa-cart-plus" style={{fontWeight: 550}}></i>
                                                               </Button>
                                                        </div>
                                                 </Row>
                                          </Card.Body>
                                   </Card>);

              return (
                     <Col  sm={4} md={6} lg={4} xl={3} xs={6} key={item.id}>
                            <ReactCardFlip isFlipped={this.state.isFlipped} flipDirection="horizontal" >
                                   <Card key="front">
                                          <Card.Img variant="top" src={item.image} className="responsive-img" />
                                          
                                          <Card.Body>
                                                 <Card.Title className="responsive-txt">{item.name}</Card.Title>
                                                 <Row>
                                                        <div className="container">
                                                               {item.description != null ?
                                                               <Button onClick={this.handleClick} className="col-3 text-left" variant="light"  size="sm">
                                                                      <i className="fas fa-info-circle" style={{fontWeight: 550}}></i>
                                                                      </Button>:''
                                                               }
                                                               <Button onClick={this.addCart} className={item.description != null ? "col-4 offset-5" : "col-4 offset-8"} variant="outline-primary" size="sm">
                                                                      <i className="fas fa-cart-plus" style={{fontWeight: 550}}></i>
                                                               </Button>
                                                        </div>
                                                 </Row>
                                          </Card.Body>
                                   </Card>
                                   {back}
                                   
                            </ReactCardFlip>
                     </Col>
                     );
              }
       }
       export default BodyCard;
       
       