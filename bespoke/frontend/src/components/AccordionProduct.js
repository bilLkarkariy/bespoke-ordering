import React from 'react';
import HorizontalNav from './HorizontalNav';
import CardCustom from './CardCustom';
import './css/style.css';
import ReactDOM from 'react-dom';
import Select from 'react-select';

class Panel extends React.Component {
      constructor(props) {
		super(props);
		this.state = {
                     height: 0,
                     pageOfItems :[]
              };
              this.handler = this.handler.bind(this);
              this.onChangePaged = this.onChangePaged.bind(this);
              this.id_option_selected = this.props.id_option_selected
              this.id_option_update = false;
              this.name_option_selected = null; 
       }
       // This method will be sent to the child component
       handler = key => {
              console.log(ReactDOM.findDOMNode(this).querySelector('.panel__inner'));
              this.props.listNameOption.map((item, keys) => {
                     if( item.name == key )
                     { 
                            this.name_option_selected = item.name;
                            this.id_option_selected =  item.id;
                            this.id_option_update = true;
                            this.UpdateHeightPanel();
                     }
              });
       }

       onChangePaged(pageOfItems) {
              this.setState({pageOfItems:pageOfItems}) ;
       }
       

	componentDidMount() {
		this.UpdateHeightPanel();
       }
       componentWillReceiveProps(nextProps){
              if(nextProps.title != this.props.title){
                     this.id_option_update = false;
                     this.id_option_selected = nextProps.id_option_selected;
              }
     }

       UpdateHeightPanel() {
              
              window.setTimeout(() => {
                     const el = ReactDOM.findDOMNode(this);
                     const height = el.querySelector('.panel__content').clientHeight;
                     this.setState({
                            height
                     });
                     window.setTimeout(() => {
                     const el = ReactDOM.findDOMNode(this);
                     const height = el.querySelector('.panel__content').clientHeight;
                     this.setState({
                            height
                     });
              }, 333);
              }, 333);
       }

	render () {
              const { option_name, listNameOption, activeTab, index, activateTab,id_option_selected } = this.props;
              if(this.id_option_update)
              {
                     //this.id_option_update = false;
              }else{
                     this.id_option_selected = this.props.id_option_selected;
              }
		const { height } = this.state;
		const isActive = activeTab === index;
		const innerStyle =  {
			height:  `${isActive ? height : 0}px`
              }
              return (
			<div className='panel'
				role='tabpanel'
				aria-expanded={ isActive }>
				<button className='panel__label '
					role='tab'
					onClick={ activateTab }>
                                   { option_name }
                                   
				</button>

				<div className='panel__inner'
					style={ innerStyle }
					aria-hidden={ !isActive }>
					<div className='panel__content'>
                                   <HorizontalNav listMenu={listNameOption} action={this.handler} selected={this.name_option_selected}/>
                                   <CardCustom optionId={this.id_option_selected} action={this.onChangePaged} 
                                          pageOfItems = {this.state.pageOfItems}  props_cart={this.props.props_cart}/>
					</div>
				</div>
			</div>
		);
	}
}


class AccordionProduct extends React.Component {
       constructor(props) {
              super(props);
              this.state = {
                     activeTab: 0,
                     loading : true,
                     id_option_selected: 1,
                     product_id:0
               };
               this.activateTab = this.activateTab.bind(this);
       }
	activateTab(index) {
		this.setState(prev => ({
			activeTab: prev.activeTab === index ? -1 : index
              }));
       }



       render() {
              
              // if (!this.state.data) {
              //        return <div>Error fetching data Accordion</div>;
              // }
              // if(this.props.data.id != this.state.product_id){
              //        this.state.product_id = this.props.data.id;
              //        this.fetchingData();
              //  }

              var listNameOption = [];
              var listNameOptionFacultative = [];
              this.props.options.map((item, key) =>
                     (!item.isFacultatif ? listNameOption.push({name:item.name,id:item.id}) : listNameOptionFacultative.push({name:item.name,id:item.id}))
              ); 

              const { activeTab } = this.state;
              const option_name = "Option " + this.props.data.name;
              const option_name_f = "Option " + this.props.data.name+ " Facultatives";
              const panels = listNameOptionFacultative.length? [listNameOption,listNameOptionFacultative] : [listNameOption] ;
              const panels_name =[option_name,option_name_f];
              
		return (
			<div className='accordion' role='tablist'>
       				{panels.map((panel, index) =>

					<Panel
                                          props_cart={this.props.props_cart}
                                          option_name={panels_name[index]}
                                          listNameOption={panel}
						key={ index }
						activeTab={ activeTab }
						index={ index }
						{ ...panel } 
                                          activateTab={ this.activateTab.bind(null, index) }
                                          id_option_selected = {panel[0].id}
                                          title={this.props.title}
                                          
					/>
                                   )}

			</div>
		);
       }
}
export default AccordionProduct;




