import NavBarCustom from "./NavBarCustom";
import DataProvider from "./ProductsProvider";
import "antd/dist/antd.css";
import lodash from "lodash";
import { Grid, Slug, Fade } from "mauerwerk";
import "./css/style.css";
import data from "./data";
import { Icon } from "antd";
import React, { Component, Fragment } from "react";
import { render } from "react-dom";

const Cell = ({ toggle, id, name, height, description, src, css, maximized, handler_id }) => (
  <div
    className="cell"
    style={{ backgroundImage: css, cursor: !maximized ? "pointer" : "auto" }}
    onClick={!maximized ? toggle : undefined}
  >
  <img src={src}  alt="Costume 3 pièces" style={{maxHeight: '80%'}}/>;
    <Fade show={maximized} delay={maximized ? 200 : 0}>
      <div className="details">
        <Slug delay={400}>
          <div className="circle" style={{ background: css }} 
          onClick={handler_id} >
                 <div className="circle-text" value={id}>
                        Selectionner
                 </div>
          </div>
          <div className="close">
            <Icon type="close" style={{ cursor: "pointer" }} onClick={toggle} />
          </div>
          <h1>{name}</h1>
          <p>{description}</p>
        </Slug>
      </div>
    </Fade>
    <Fade
      show={!maximized}
      from={{ opacity: 0, transform: "translate3d(0,140px,0)" }}
      enter={{ opacity: 1, transform: "translate3d(0,0px,0)" }}
      leave={{ opacity: 0, transform: "translate3d(0,-50px,0)" }}
      delay={maximized ? 0 : 200}
    >
      <div className="default">{name}</div>
    </Fade>
  </div>
);
/**
 *
 */
class MainContainer extends React.Component {
  /**
   * @param state : {product_index : index (not id) of the products : {Veste, Pantalon, Chemise, Gilet, Jupe, ...} }
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      product_index: 0,
      data,
      columns: 3,
      margin: 70,
      filter: "",
      height: true,
      isSelected:false
    };

    this.cart = [];
    this.cart_update = false;
    /**
     * Binding handler methode
     */
    this.handler = this.handler.bind(this);
    this.handler_id = this.handler_id.bind(this);
    this.props_cart = this.props_cart.bind(this);
  }
  props_cart = e => {
    this.setState({ test: e });

    this.cart.forEach((element, index) => {
      if (element.option_link == e.option_link) {
        this.cart.splice(index, 1);
      }
    });
    this.cart.push(e);
  };

  // When user click on the product Index nav bar item the app update DataProvider
  handler = e => {
    const product_index = Number.parseInt(e.target.href.slice(-1));
    this.setState({ product_index: product_index });
    this.cart = [];
  };
  // When user click on the product Index nav bar item the app update DataProvider
  handler_id = e => {
       this.setState({ isSelected: true });
       console.log(e.target["value"])
       console.log(e.target.value)
  };
  render() {
    const url = "api-bespoke/products/";
    const data = this.state.data.filter(
      d => d.name.toLowerCase().indexOf(this.state.filter) != -1
    );
    const isSelected = this.state.isSelected;
    return (
      <div className="full-width">
        {/* A custom NavBar component base on React Boostrap */}
        <NavBarCustom
          product_index={this.state.product_index}
          handler={this.handler}
          url={url}
        />


          {/* Data Provider is the component in charge of querying api-bespoke for products   */}
        {true?
        <DataProvider
          product_index={this.state.product_index}
          url={url}
          cart_update={this.cart_update}
          props_cart={this.props_cart}
          cart={this.cart}
        />
        :
                <div className="main">
          <Grid
            className="grid"
            // Arbitrary data, should contain keys, possibly heights, etc.
            data={data}
            // Key accessor, instructs grid on how to fet individual keys from the data set
            keys={d => d.name}
            // Can be a fixed value or an individual data accessor
            heights={this.state.height ? d => d.height : 200}
            // Number of columns
            columns={this.state.columns}
            // Space between elements
            margin={this.state.margin}
            // Removes the possibility to scroll away from a maximized element
            lockScroll={true}
            // Delay when active elements (blown up) are minimized again
            closeDelay={200}
          >
            {(data, maximized, toggle) => (
              <Cell {...data} maximized={maximized} toggle={toggle}  handler_id={this.handler_id}  />
            )}
          </Grid>
        </div>
       
        }
       
      </div>
    );
  }
}
export default MainContainer;
