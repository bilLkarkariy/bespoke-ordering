import React from "react";
import ReactDOM from "react-dom";
import MainContainer from "./MainContainer";


/////////////// App js entry point of react dom
import './css/style.css';

const App = () => {
    return (
            <MainContainer/>
            )
    }
const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : null;