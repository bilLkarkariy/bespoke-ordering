import {Container,Row}  from 'react-bootstrap'
import React from "react";
import BodyCard from './BodyCard';
import Pagination from './Pagination'

/**
 * Custom Card class 
 */
class CardCustom extends React.Component {
       
       /**
       * @param state = {
       * @property loading: is DataProfider queyring is still loading  
       * @property items: array storing data from data fetching, data stored is 
                      the resonspe of the api-bespoke/option/id/items which returns all items fields of th id options
       * @property pageSize:    how many Items to display in each page
       * @property optionId:   The current option_id selected
       * 
       * @param {*} props 
       */
       constructor(props) {
              super(props);
              this.state = {
                     items : null,
                     loading : true,
                     pageSize:8
              };       
       }
       async componentWillMount() {
              await this.fetchingData( this.props.optionId);
       }
       async componentWillReceiveProps(nextProps){
                 if(nextProps.optionId != this.props.optionId){
                     console.log("CardCustom");
                     
                     await this.fetchingData( nextProps.optionId);
                 }
        }

       async fetchingData(id) {
              const url = "/api-bespoke/options/" +id + "/items/";
              const response = await fetch(url);
              const data_response = await response.json();
              this.setState({ items: data_response.results, loading: false });
       }

       render() {
              if(this.state.loading){
                     return <p>loading ...</p>;
              }
              if (!this.state.items) {
              return <div>Error fetching data</div>;
              }
              // if(this.props.optionId != this.state.optionId){
              //       this.state.optionId = this.props.optionId;
              //       this.fetchingData();
              // }
              let items = [];
              this.props.pageOfItems.map((item, key) =>
              items.push(<BodyCard item={item} key={key} props_cart={this.props.props_cart}/>)
              )
              return (
                     <Container fluid style={{ paddingLeft: 0, paddingRight: 0, marginBottom:20 }} >
                            <Row>
                                   {items}
                            </Row>
                            <Pagination items={this.state.items} pageSize={this.state.pageSize} onChangePage={this.props.action} />
                     </Container>
              );
       }
}
export default CardCustom;






