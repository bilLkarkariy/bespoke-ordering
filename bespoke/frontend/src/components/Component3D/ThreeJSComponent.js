import React from "react";
import * as THREE from "three-full";
import TWEEN from "@tweenjs/tween.js";
import { Button, IconGroup } from "semantic-ui-react";

var winWidth =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth,
  winHeight =
    window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight,
  isIE =
    "Microsoft Internet Explorer" == navigator.appName ||
    !!navigator.userAgent.match(/MSIE 10./) ||
    !!navigator.userAgent.match(/Trident\/7\./),
  isIEOldVersion =
    9 >= parseInt(navigator.userAgent.substring(30, 31)) &&
    5 <= parseInt(navigator.userAgent.substring(30, 31)),
  isIE10 = !!navigator.userAgent.match(/MSIE 10./),
  isIE11 = !!navigator.userAgent.match(/Trident\/7\./),
  isMobile = 800 > winWidth,
  isTouch = "ontouchstart" in window,
  debug = 0,
  lic = 0;
const BasePath = "https://www.atelierna.com/";

Image.prototype.load = function(p) {
  var e = this,
    g = new XMLHttpRequest();
  return "onprogress" in g
    ? (g.open("GET", p, !0),
      (g.responseType = "blob"),
      (g.onload = function(g) {
        e.src = window.URL.createObjectURL(this.response);
        e.loaded(g);
      }),
      (g.onprogress = function(g) {
        e.onprogree(g);
      }),
      (g.onloadstart = function() {
        e.completedPercentage = 0;
      }),
      g.send(),
      g)
    : !1;
};
Image.prototype.loaded = function(p) {};
Image.prototype.onprogree = function(p) {};
Image.prototype.completedPercentage = 0;

const starter_veste = [
  ["fabric", "HS172050"],
  ["jacket_button", "BS003"],
  ["2_buttons", "HS172050"]
];

//data autoselected chemise
const starter_chemise = [["fabric", "HS172050"]];
/*************************************************** */
//data autoselected chemise
const starter_gilet = [["fabric", "HS172050"]];
const starter_pantalon = [["fabric", "HS172050"]];
/*************************************************** */

class LoaderTexture {
  constructor(b) {
    var e = this;
    this.progress = 0;
    this.texturePath = b;
    this.image = new Image();
    var a = null;
    this.progressTexture = function(a) {
      e.onProgress(a);
    };
    this.onProgress = function(a) {};
    this.onLoaded = function() {};
    this.abort = function() {
      null != a && (0 == a ? (e.image.src = "") : (a.abort(), delete e.image));
    };
    this.destroy = function() {
      null != a && (0 == a ? (e.image.src = "") : (a.abort(), delete e.image));
    };
    (() => {
      e.image.onload = function() {
        e.onLoaded();
      };
      e.image.loaded = function(a) {};
      e.image.onprogree = function(a) {
        e.progressTexture(a);
      };
      a = e.image.load(e.texturePath);
      0 == a &&
        ((e.image.src = e.texturePath),
        e.progressTexture({
          loaded: 100,
          total: 100
        }));
    })();
  }




}

class Loader3D {
  constructor(modele, scene) {
    this.progress = 0;
    this.modele = modele;
    var g = null,
      c = new THREE.OBJLoader();
    this.progressCallback = b => {
      this.progress = (b.loaded / b.total) * 100;
      this.onProgress(b);
    };
    this.onProgress = function(a) {};
    this.onLoaded = function() {};
    this.abort = function() {
      null != g && (g.abort(), (g = null), delete c.manager, (c = null));
    };
    this.destroy = function() {
      null != g && (g.abort(), (g = null));
      delete c.manager;
      c = null;
    };
    (() => {
      g = c.load(
        "static/" + this.modele + ".obj",
        b => {
          scene.add(b);
          b.position.set(0, 0.7, 0);
          b.name = this.modele;
          b.visible = !1;

          this.onLoaded();
        },
        this.progressCallback
      );
    })();
  }
}

class Zoom {
  constructor(camera, product) {
    this.camera = camera;
    this.product = product;
    this.zoomLevel = 0;
    var h = this;
    this.positionZoom1 = {
      x: -4.777360071248828,
      y: 6.230226798935897,
      z: 7.9436621565374,
      _x: -0.13660868921408284,
      _y: -0.525157906959069,
      _z: -0.06665107759271915
    };
    this.positionZoom2 = {
      x: -3.42736007124883,
      y: 6.280226798935897,
      z: 5.843662156537411,
      _x: -0.13660868921408284,
      _y: -0.525157906959069,
      _z: -0.06665107759271915
    };
    this.positionZoom3 = {
      x: -2.42736007124883,
      y: 6.280226798935897,
      z: 4.843662156537411,
      _x: -0.13660868921408284,
      _y: -0.525157906959069,
      _z: -0.06665107759271915
    };
    this.positionZoom1Pant = {
      x: -5.741682959713684,
      y: 3.99426291138707,
      z: 12.633667200533925,
      _x: -0.13781884703657993,
      _y: -0.41399442916942797,
      _z: -0.055736235211798796
    };
    this.positionZoom2Pant = {
      x: -4.777360071248828,
      y: 4.230226798935897,
      z: 7.9436621565374,
      _x: -0.13660868921408284,
      _y: -0.525157906959069,
      _z: -0.06665107759271915
    };
    this.positionZoom3Pant = {
      x: -3.42736007124883,
      y: 4.180226798935904,
      z: 5.843662156537411,
      _x: -0.13660868921408284,
      _y: -0.525157906959069,
      _z: -0.06665107759271915
    };
    this.setZoomLevel = function(a) {
      this.zoomLevel = a;
    };
    this.zoomIn = () => {
      this.a(1);
    };
    this.zoomOut = () => {
      this.a(-1);
    };
    this.a = function(a) {
      if ((0 < this.zoomLevel && -1 == a) || (3 > this.zoomLevel && 1 == a))
        switch (((this.zoomLevel += a), this.zoomLevel)) {
          case 0:
            if (-1 == a) {
              var c = {
                x: this.camera.position.x,
                y: this.camera.position.y,
                z: this.camera.position.z,
                _x: this.camera.rotation.x,
                _y: this.camera.rotation.y,
                _z: this.camera.rotation.z
              };
              a = new TWEEN.Tween(c)
                .to(q, 2e3)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                  this.camera.position.x = c.x;
                  this.camera.position.y = c.y;
                  this.camera.position.z = c.z;
                  this.camera.rotation.x = c._x;
                  this.camera.rotation.y = c._y;
                  this.camera.rotation.z = c._z;
                });
              a.start();
            }
            break;
          case 1:
            1 == a
              ? ((c = {
                  x: this.camera.position.x,
                  y: this.camera.position.y,
                  z: this.camera.position.z,
                  _x: this.camera.rotation.x,
                  _y: this.camera.rotation.y,
                  _z: this.camera.rotation.z
                }),
                (a =
                  null != this.product && "pantalon" != this.product.modele
                    ? h.positionZoom1
                    : h.positionZoom1Pant),
                new TWEEN.Tween(c)
                  .to(a, 2e3)
                  .easing(TWEEN.Easing.Quadratic.Out)
                  .onUpdate(() => {
                    this.camera.position.x = c.x;
                    this.camera.position.y = c.y;
                    this.camera.position.z = c.z;
                    this.camera.rotation.x = c._x;
                    this.camera.rotation.y = c._y;
                    this.camera.rotation.z = c._z;
                  })
                  .start())
              : ((c = {
                  x: this.camera.position.x,
                  y: this.camera.position.y,
                  z: this.camera.position.z,
                  _x: this.camera.rotation.x,
                  _y: this.camera.rotation.y,
                  _z: this.camera.rotation.z
                }),
                (a =
                  null != this.product && "pantalon" != this.product.modele
                    ? h.positionZoom1
                    : h.positionZoom1Pant),
                (a = new TWEEN.Tween(c)
                  .to(a, 2e3)
                  .easing(TWEEN.Easing.Quadratic.Out)
                  .onUpdate(() => {
                    this.camera.position.x = c.x;
                    this.camera.position.y = c.y;
                    this.camera.position.z = c.z;
                    this.camera.rotation.x = c._x;
                    this.camera.rotation.y = c._y;
                    this.camera.rotation.z = c._z;
                  })),
                a.start());
            break;
          case 2:
            c = {
              x: this.camera.position.x,
              y: this.camera.position.y,
              z: this.camera.position.z,
              _x: this.camera.rotation.x,
              _y: this.camera.rotation.y,
              _z: this.camera.rotation.z
            };
            a =
              null != this.product && "pantalon" != this.product.modele
                ? h.positionZoom2
                : h.positionZoom2Pant;
            new TWEEN.Tween(c)
              .to(a, 2e3)
              .easing(TWEEN.Easing.Quadratic.Out)
              .onUpdate(() => {
                this.camera.position.x = c.x;
                this.camera.position.y = c.y;
                this.camera.position.z = c.z;
                this.camera.rotation.x = c._x;
                this.camera.rotation.y = c._y;
                this.camera.rotation.z = c._z;
              })
              .start();
            break;
          case 3:
            1 == a &&
              ((c = {
                x: this.camera.position.x,
                y: this.camera.position.y,
                z: this.camera.position.z,
                _x: this.camera.rotation.x,
                _y: this.camera.rotation.y,
                _z: this.camera.rotation.z
              }),
              (a =
                null != this.product && "pantalon" != this.product.modele
                  ? h.positionZoom3
                  : h.positionZoom3Pant),
              new TWEEN.Tween(c)
                .to(a, 2e3)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                  this.camera.position.x = c.x;
                  this.camera.position.y = c.y;
                  this.camera.position.z = c.z;
                  this.camera.rotation.x = c._x;
                  this.camera.rotation.y = c._y;
                  this.camera.rotation.z = c._z;
                })
                .start());
        }
    };
  }
}

class ThreeJSComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    //data autoselected veste

    this.modele = props.title.toLowerCase();
    this.THREE = THREE;
    this.processMachine = new Array();
    this.camera = null;
    this.isInitialized = false;
    this.product = null;
    //data autoselected for starting display
    this.data = starter_veste;
    //default fabric tissus ref
    this.fabric = "HS172050";
    this.button_fabric = "BS003";
    this.defaultTexture = new LoaderTexture("media/textures/default.jpg");
    this.maskTexture = new LoaderTexture("media/textures/mask.png");

    // this.data['chemise'] = starter_chemise;
    // this.data['gilet']   = starter_gilet;
    // this.data['pantalon']   = starter_pantalon;
    /*************************************************** */
    var canvas, renderer, r, u;
    (this.canvasID = "canvas"), (this.x = document.createElement("canvas"));
    this.y = document.createElement("canvas");
    this.D = document.createElement("canvas");
    this.A = document.createElement("canvas");
    this.C = document.createElement("canvas");
    this.E = document.createElement("canvas");
    this.I = 0;
    var e = this;

    var b,
      B,
      q,
      H = !1;
    var F,
      v,
      w,
      t = (function() {
        try {
          var b = document.createElement("canvas");
          return !(
            !window.WebGLRenderingContext ||
            !(
              b.getContext("webgl") ||
              b.getContext("experimental-webgl") ||
              b.getContext("moz-webgl") ||
              b.getContext("webkit-3d")
            )
          );
        } catch (e) {
          return !1;
        }
      })()
        ? new THREE.WebGLRenderer({
            antialias: !0,
            alpha: !0
          })
        : !1;

    this.is3DSupported = !0;
    this.zoomer = this.product = this.scene = null;
    this.setControl = !1;
    this.screenshotColor = "#CCCCCC";
  }
  onMouseDown() {}
  onMouseMove() {}
  onMouseUp() {}
  zoomIn() {
    this.setControl &&
      this.zoomer &&
      this.product.modeleGroup &&
      this.zoomer.zoomIn();
  }
  zoomOut() {
    this.setControl &&
      this.zoomer &&
      this.product.modeleGroup &&
      this.zoomer.zoomOut();
  }
  init() {
    this.renderer = new THREE.WebGLRenderer({
      antialias: !0,
      alpha: !0
    });
    if (!this.renderer || (isIE && isIEOldVersion))
      return (this.is3DSupported = !1);

    this.x.width = this.r;
    this.x.height = this.u;
    this.renderer.setClearColor(0, 0);
    this.renderer.setPixelRatio(2);
    this.renderer.setSize(this.r, this.u);
    this.renderer.gammaInput = !1;
    this.renderer.gammaOutput = !1;
    this.canvas.appendChild(this.renderer.domElement);
    this.canvas.style.overflow = "hidden";
    this.canvas.style.height = "auto";

    var m = Math.round(
      (this.canvas.offsetWidth - this.canvas.children[0].offsetWidth) / 2
    );
    this.canvas.children[0].style.marginTop =
      -Math.round(
        this.canvas.offsetHeight - this.canvas.children[0].offsetHeight
      ) + "px";
    this.canvas.children[0].style.marginLeft = m + "px";
    this.camera = new THREE.PerspectiveCamera(40, this.r / this.u, 1, 1e4);
    //this.controls = new OrbitControls( this.camera );

    this.scene = new THREE.Scene();

    window.addEventListener("resize", a => {
      this.resizeRender();
    });
    this.anisotropy = 16;
    this.camera.position.x = -6.0;
    this.camera.position.y = 6.6;
    this.camera.position.z = 16.33;
    this.camera.rotation.x = -0.178;
    this.camera.rotation.y = -0.337;
    this.camera.rotation.z = -0.059;
    window.requestAnimFrame = (function() {
      return (
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function(a) {
          window.setTimeout(a, 1e3 / 60);
        }
      );
    })();

    this.animate();
    this.setControl = !0;
  }
  screenshot(b) {
    b = "undefined" != typeof b ? b : 0.8;
    H = !0;
    this.animate();
    H = !1;
    return x.toDataURL("image/jpeg", b);
  }
  setRotation(restart) {
    var b = 0;
    restart ? (b = 0) : (b = 120);
    var m = {
      _y: this.product.modeleGroup.rotation.y
    };
    if (this.product && this.product.modeleGroup) {
      var c =
        2 *
          Math.round(this.product.modeleGroup.rotation.y / (2 * Math.PI)) *
          Math.PI +
        (b * Math.PI) / 180;
      c = m._y + (c % (2 * Math.PI));
      new TWEEN.Tween(m)
        .to(
          {
            _y: c
          },
          1e3
        )
        .easing(TWEEN.Easing.Quadratic.InOut)
        .onUpdate(a => {
          this.product.modeleGroup.rotation.y = m._y;
        })
        .onComplete(function() {})
        .start();
    } else return !1;
  }
  setVisible(b) {
    "undefined" != typeof b &&
      this.product &&
      this.product.modeleGroup &&
      (this.product.modeleGroup.visible = b);
  }
  onProgress(b) {}
  loaded() {
    setTimeout(() => {
      this.zoomer.setZoomLevel(1);
      this.zoomer.zoomIn();
    }, 100);
  }
  upateTweenPosition() {
    var e = {
      x: -6.002544966739292,
      y: 5.241570249194899,
      z: 6.119111284229684,
      _x: -0.18515587520161453,
      _y: -0.29807250641988153,
      _z: -0.05495090593940036
    };
    new TWEEN.Tween(e)
      .to(
        {
          x: -6.024712165036524,
          y: 6.645165764553804,
          z: 16.336940064604498,
          _x: -0.178875580396339,
          _y: -0.3375103887785267,
          _z: -0.059801226178455194
        },
        3e3
      )
      .easing(TWEEN.Easing.Quadratic.Out)
      .onUpdate(() => {
        this.camera.position.x = e.x;
        this.camera.position.y = e.y;
        this.camera.position.z = e.z;
        this.camera.rotation.x = e._x;
        this.camera.rotation.y = e._y;
        this.camera.rotation.z = e._z;
      })
      .start();
    var h = {
        _y: -4,
        opa: 0,
        opa2: 0
      },
      a = {
        _y: 0,
        opa: 0.2,
        opa2: 1
      };
    new TWEEN.Tween(h)
      .to(a, 4e3)
      .easing(TWEEN.Easing.Quadratic.Out)
      .onUpdate(() => {
        mannequin.rotation.y = h._y;
      });
    var m = new TWEEN.Tween(a)
        .to(
          {
            _y: -0.3
          },
          800
        )
        .delay(700)
        .easing(TWEEN.Easing.Quadratic.InOut)
        .onUpdate(() => {
          mannequin.rotation.y = a._y;
        }),
      c = new TWEEN.Tween(a)
        .to(
          {
            _y: 0.3
          },
          1e3
        )
        .easing(TWEEN.Easing.Quadratic.InOut)
        .onUpdate(() => {
          mannequin.rotation.y = a._y;
        }),
      g = new TWEEN.Tween(a)
        .to(
          {
            _y: 0
          },
          800
        )
        .delay(100)
        .easing(TWEEN.Easing.Quadratic.InOut)
        .onUpdate(() => {
          mannequin.rotation.y = a._y;
        })
        .onComplete(function() {});
    m.chain(c);
    c.chain(g);
  }
  animate() {
    //this.upateTweenPosition();
    TWEEN.update();
    this.renderer.render(this.scene, this.camera);
    this.H &&
      ((this.x.getContext("2d").fillStyle = this.screenshotColor),
      this.x.getContext("2d").fillRect(0, 0, this.r, this.u),
      this.x
        .getContext("2d")
        .drawImage(this.renderer.domElement, 0, 0, this.r, this.u));
    requestAnimationFrame(this.animate.bind(this));
  }
  resizeRender() {
    this.r =
      1e3 > this.canvas.offsetWidth
        ? 1e3
        : 1500 < this.canvas.offsetWidth
        ? 1500
        : this.canvas.offsetWidth;
    this.u = Math.round(this.r / 1.3);
    winWidth =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    winHeight =
      window.innerHeight * 0.9 ||
      document.documentElement.clientHeight * 0.9 ||
      document.body.clientHeight * 0.9;
    isMobile = 800 > winWidth;
    this.camera.aspect = this.r / this.u;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.r, this.u);
    this.x.width = this.r;
    this.x.height = this.u;
    var e = Math.round(
      (this.canvas.offsetWidth - this.canvas.children[0].offsetWidth) / 2
    );
    this.canvas.children[0].style.marginTop =
      -Math.round(
        this.canvas.offsetHeight - this.canvas.children[0].offsetHeight
      ) + "px";
    this.canvas.children[0].style.marginLeft = e + "px";
  }

  destroy() {
    this.product && this.product.destroy(); //, delete this.product);
    //window.requestAnimFrame = function() {};
    delete this.zoomer;
    //delete this.animate;
    delete this.scene;
  }

  getOptionById(option_link) {
    for (var i = 0; this.props.options.length; i++) {
      if (this.props.options[i].id == option_link) return this.props.options[i];
    }
    return "";
  }

  getOptionRefById(option_link) {
    var options = this.props.options;
    for (var k = 0; k < options.length; ++k) {
      if ((options[k].id, option_link)) {
        return options[k].ref;
      }
    }
    return null;
  }
  getTissusRef() {
    if (this.data.length > 0) {
      if (this.data[0][0] == "fabric") return this.data[0][1];
    }
    return this.fabric;
  }

  updateVeste(cart) {
    var containsReplcementItem = null;
    var is_slanting = false;
    var btn_type_selected = false;
    cart.forEach(item => {
      if (item.ref == "3_buttons") {
        containsReplcementItem = "small_";
      } else if (
        item.ref == "cross_2_buttons" ||
        item.ref == "cross_4_buttons"
      ) {
        containsReplcementItem = "cross_";
      } else if (item.ref == "slanting") {
        is_slanting = true;
      }
      if (this.getOptionById(item.option_link).ref == "jacket_type") {
        btn_type_selected = true;
      }
      if (item.ref.split(";").length > 1) {
        var refs = item.ref.split(";");
        refs.forEach(ref => {
          this.data.push([ref, this.getTissusRef(item)]);
        });
      } else {
        if (item.ref == "_button") {
          this.data.push(["jacket" + item.ref, item.name]);
        } else {
          this.data.push([item.ref, this.getTissusRef(item)]);
        }
      }
    });
    if (containsReplcementItem != null) {
      this.data.forEach((item, index) => {
        if (item[0].includes("_reverse") || item[0].includes("shawl")) {
          if (containsReplcementItem == "cross_") {
            item[0] = containsReplcementItem + item[0];
            if (item[0].includes("_reverse") && index > 0) {
              this.data[index - 1][0] =
                containsReplcementItem + this.data[index - 1][0];
            }
          } else {
            item[0] = containsReplcementItem + item[0];
          }
        }
      });
    }

    if (is_slanting) {
      this.data.forEach((element, index) => {
        if (element[0] == "flap" || element[0] == "double_piping") {
          element[0] += "_slanting";
        } else if (element[0] == "slanting") {
          this.data.splice(index, 1);
        }
      });
    }
    console.log(btn_type_selected);

    if (!btn_type_selected) {
      this.data.push(["1_button", this.fabric]);
    }
  }
  updatePantalon(cart) {
    var is_piping = false;
    cart.forEach(item => {
      console.log(item);

      if (item.ref == "pipping") {
        is_piping = true;
      } else {
        if (item.ref.split(";").length > 1) {
          var refs = item.ref.split(";");
          console.log("refs : ", refs);

          refs.forEach(ref => {
            this.data.push([ref, this.getTissusRef(item)]);
          });
        } else if (item.ref != "no_pipping" && item.ref != "no_3D") {
          if (item.ref == "_button") {
            this.data.push(["pants" + item.ref, item.name]);
          } else {
            this.data.push([item.ref, this.getTissusRef(item)]);
          }
        }
      }
    });

    if (is_piping) {
      this.data.forEach((item, index) => {
        console.log(item);
        if (
          item[0] == "pants_back_pocket_left" ||
          item[0] == "pants_back_pocket_right"
        ) {
          item[0] = item[0].slice(0, 17) + "_pipping" + item[0].slice(17);
        } else if (item[0] == "pipping") {
          this.data.splice(index, 1);
        }
      });
    }
  }

  updateProcessMachine(cart) {
    console.log(cart, this.modele);
    this.data = new Array(0);
    //this.data.splice(0,1);
    console.log(this.data);

    if (cart.length == 0) {
      this.data.push(["fabric", this.fabric]);
      return;
    }
    if (this.getOptionById(cart[cart.length - 1].option_link).ref == "fabric") {
      this.data.push(["fabric", cart[cart.length - 1].ref]);
      this.fabric = cart[cart.length - 1].ref;
    } else {
      this.data.push(["fabric", this.fabric]);
    }
    if (this.modele == "veste") {
      this.updateVeste(cart);
    } else if (this.modele == "pantalon") {
      this.updatePantalon(cart);
    }
  }

  componentDidMount() {
    this.canvas = document.getElementById("canvas");
    this.initWebApp();
  }
  initWebApp() {
    (this.r =
      1e3 > this.canvas.offsetWidth
        ? 1e3
        : 2e3 < this.canvas.offsetWidth
        ? 1500
        : this.canvas.offsetWidth),
      (this.u = Math.round(this.r / 1.3));
    this.camera = new THREE.PerspectiveCamera(40, this.r / this.u, 1, 1e4);
    this.init();
    this.product = new Product(this);
    this.zoomer = new Zoom(this.camera, this.product);
    this.addEventMouseListener();
  }

  addEventMouseListener() {
    var G = a => {
        isTouch
          ? (document
              .getElementById(this.canvasID)
              .removeEventListener("touchmove", h),
            document
              .getElementById(this.canvasID)
              .removeEventListener("touchend", G))
          : (a.preventDefault(),
            document.removeEventListener("mousemove", h),
            document.removeEventListener("mouseup", G));
        this.onMouseUp();
      },
      h = a => {
        // isTouch&&void 0===a.touches&&void 0!==a.originalEvent?a.touches=a.originalEvent.touches:a.preventDefault();
        var b =
          J._y +
          2 *
            Math.PI *
            (((isTouch ? a.touches[0].pageX : a.pageX) - c) /
              document.getElementById(this.canvasID).offsetWidth);
        k && k.stop();
        k = new TWEEN.Tween({
          _y: this.product.modeleGroup.rotation.y
        })
          .to(
            {
              _y: b
            },
            1e3
          )
          .onUpdate(() => {
            this.product.modeleGroup.rotation.y +=
              (b - this.product.modeleGroup.rotation.y) / 20;
          });
        this.onMouseMove();
        k.start();
      },
      touchStart = a => {
        this.setControl &&
          this.product &&
          this.product.modeleGroup &&
          (isTouch
            ? (void 0 === a.touches &&
                void 0 !== a.originalEvent &&
                (a.touches = a.originalEvent.touches),
              document
                .getElementById(this.canvasID)
                .addEventListener("touchmove", h),
              document
                .getElementById(this.canvasID)
                .addEventListener("touchend", G))
            : (a.preventDefault(),
              document.addEventListener("mousemove", h),
              document.addEventListener("mouseup", G)),
          (c = isTouch ? a.touches[0].pageX : a.pageX),
          (J = {
            _x: this.product.modeleGroup.rotation.x,
            _y: this.product.modeleGroup.rotation.y,
            _z: this.product.modeleGroup.rotation.z
          }),
          this.onMouseDown());
      };
    var c, J, k;
    isTouch
      ? document
          .getElementById(this.canvasID)
          .addEventListener("touchstart", touchStart)
      : document
          .getElementById(this.canvasID)
          .addEventListener("mousedown", touchStart);
  }

  componentWillMount() {
    console.log(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (this.modele != nextProps.title) {
      this.destroy();
      this.modele = nextProps.title.toLowerCase();
      this.data = this.product["starter_" + this.modele];

      document.getElementsByTagName("CANVAS")[0].remove();
      this.initWebApp();
    } else {
      this.updateProcessMachine(nextProps.cart);
      this.product.set(this.data);
    }
  }
  render() {
    return (
      <div id="canvas" className="canvas">
        <Button.Group
          vertical
          size="small"
          secondary
          labeled
          inverted
          icon
          compact
          className="HUD-btn"
        >
          <Button
            onClick={() => {
              this.zoomIn();
            }}
            icon="zoom-in"
            content="Zoom In"
          />
          <Button
            onClick={() => {
              this.zoomOut();
            }}
            icon="zoom-out"
            content="Zoom Out"
          />
          <Button
            onClick={() => {
              this.setRotation();
            }}
            icon="sync alternate"
            content="Rotate"
          />
        </Button.Group>{" "}
      </div>
    );
  }
}
export default ThreeJSComponent;

class Product {
  constructor(ThreeJSComponent) {
    this.modele = ThreeJSComponent.modele || "veste";
    this.data = ThreeJSComponent.data || [];

    this.modeleGroup = null;
    var a = this,
      e = ThreeJSComponent,
      m = 0,
      c = [],
      g = 0;
    this.scene = ThreeJSComponent.scene;

    console.log("data :",this.data);

    this.set = function(a) {
      this.data = a;
      this.p();
    };
    this.display = function() {
      this.createTexture();
    };
    this.createTexture = function() {
      var x = e.x;
      var y = e.y;
      var D = e.D;
      var A = e.A;
      var C = e.C;
      var E = e.E;
      var F = e.F;
      var I = e.I;
      var B = e.anisotropy;
      var b = null,
        w = e.w,
        v = e.v,
        F = e.F,
        c = y.getContext("2d"),
        m = D.getContext("2d"),
        ctx = D.getContext("2d"),
        g = !1;
      y.width = 2048;
      y.height = 2048;
      D.width = 2048;
      D.height = 2048;
      c.clearRect(0, 0, y.width, y.height);
      m.clearRect(0, 0, y.width, y.height);
      ctx.clearRect(0, 0, y.width, y.height);
      this.order();

      for (
        var l = this.scene.getObjectByName(this.modele + "_bouton"),
          f = 0,
          h = l.children.length;
        f < h;

      )
        "ground" !=
          this.scene.getObjectByName(this.modele + "_bouton").children[f]
            .name && (l.children[f].visible = !1),
          ++f;
      "undefined" != typeof this.scene.getObjectByName("bouton") &&
        (this.scene.getObjectByName("bouton").visible = !0);
      "chemise" == this.modele &&
        ((this.scene.getObjectByName("collong").visible = !1),
        (this.scene.getObjectByName("col").visible = !0));
      "veste" == this.modele &&
        ((this.scene.getObjectByName("colHaut").visible = !1),
        (this.scene.getObjectByName("croise").visible = !1));

      f = 0;

      for (h = a.data.length; f < h; ) {
        if (a.data[f][7] == this.modele) {
          if (a.data[f][2]) {
            var l =
                "" == a.data[f][6]
                  ? 0
                  : "undefined" != typeof a.data[f][6].x
                  ? a.data[f][6].x
                  : 0,
              n =
                "" == a.data[f][6]
                  ? 0
                  : "undefined" != typeof a.data[f][6].y
                  ? a.data[f][6].y
                  : 0,
              K = 1;
            if (a.data[f][4]) {
              "veste" != a.data[f][7] ||
                ("small_large_peaked_reverse_border" != a.data[f][0] &&
                  "small_peaked_reverse_border" != a.data[f][0] &&
                  "small_thin_peaked_reverse_border" != a.data[f][0] &&
                  "small_large_peaked_reverse" != a.data[f][0] &&
                  "small_peaked_reverse" != a.data[f][0] &&
                  "small_thin_peaked_reverse" != a.data[f][0] &&
                  "small_large_notched_reverse" != a.data[f][0] &&
                  "small_notched_reverse" != a.data[f][0] &&
                  "small_thin_notched_reverse" != a.data[f][0] &&
                  "small_large_notched_reverse_border" != a.data[f][0] &&
                  "small_notched_reverse_border" != a.data[f][0] &&
                  "small_thin_notched_reverse_border" != a.data[f][0]) ||
                ((K = 0.82), (n += 25), (l += 1));
              A.width = a.data[f][4].image.width;
              A.height = a.data[f][4].image.height;
              var z = A.getContext("2d");

              z.drawImage(
                a.data[f][4].image,
                0,
                0,
                a.data[f][4].image.width,
                a.data[f][4].image.height
              );
              z.globalCompositeOperation = "source-in";
              z.drawImage(
                a.data[f][2].image,
                0,
                0,
                a.data[f][2].image.width,
                a.data[f][2].image.height
              );
              c.drawImage(
                A,
                l,
                n,
                a.data[f][2].image.width,
                a.data[f][2].image.height * K
              );
              z.clearRect(0, 0, A.width, A.height);
            } else {
              var width = a.data[f][2].image.width,
                height= a.data[f][2].image.height;
              console.log("pass", l , n,  width,
                height);

              c.drawImage(
               a.data[f][2].image,
                l,
                n,
                width,
                height
              );
            //    ctx.drawImage(
            //    e.defaultTexture.image,
            //     l,
            //     n,
            //     e.defaultTexture.image.width,
            //     e.defaultTexture.image.height
            //   );
            //   var imageData = c.getImageData(0, 0, a.data[f][2].image.width,a.data[f][2].image.height );
            //   var data = imageData.data;

            //   var imageDataMain = ctx.getImageData(0, 0, e.defaultTexture.image.width,e.defaultTexture.image.height );
            //   var mainData = imageDataMain.data;
            // var z = A.getContext("2d");

            //   z.drawImage(
            //    e.maskTexture.image,
            //     l,
            //     n,
            //     e.maskTexture.image.width,
            //     e.maskTexture.image.height
            //   );

            //   var maskData = z.getImageData(0, 0, e.maskTexture.image.width,e.maskTexture.image.height );
            //   var mask = maskData.data;

            //   for (var i = 0; i < data.length; i += 4) {

            //     data[i]     = mask[i] < 1 ? data[i] : mainData[i];     // rouge
            //     data[i + 1] = mask[i+1] < 1 ? data[i+1]: mainData[i+1];// vert
            //     data[i + 2] = mask[i+2] < 1 ? data[i+2] :  mainData[i+2]; // bleu
            //   }
            //   c.putImageData(imageData, 0, 0);


            }
          }
          a.data[f][9] &&
            (m.drawImage(a.data[f][9].image, 0, 0, 2048, 2048),
            (m.globalCompositeOperation = "difference"),
            (g = !0),
            "chemise" != a.data[f][7] ||
              ("elbow_without_reverse" != a.data[f][0] &&
                "elbow_with_reverse" != a.data[f][0] &&
                "biceps_with_reverse" != a.data[f][0] &&
                "biceps_without_reverse" != a.data[f][0]) ||
              (this.scene.getObjectByName("bouton").visible = !1));

          if (a.data[f][8]) {
            if ("object" === typeof a.data[f][8]) {
              for (l = a.data[f][8].length; l--; )
                this.scene.getObjectByName(a.data[f][8][l]).visible = !0;
            } else this.scene.getObjectByName(a.data[f][8]).visible = !0;
            "chemise" == this.modele &&
              "boutonColAmericain" == a.data[f][8] &&
              ((this.scene.getObjectByName("collong").visible = !0),
              (this.scene.getObjectByName("col").visible = !1));
          }
        }
        "bouton" == a.data[f][7] && a.data[f][2] && (b = a.data[f][2].image);
        ++f;
      }

      F = new THREE.Texture(y);

      F.needsUpdate = !0;
      g &&
        ((w = new THREE.Texture(D)),
        (w.anisotropy = B),
        (w.minFilter = THREE.LinearMipMapLinearFilter),
        (w.magFilter = THREE.LinearFilter),
        (w.wrapS = THREE.ClampToEdgeWrapping),
        (w.wrapT = THREE.ClampToEdgeWrapping),
        (w.needsUpdate = !0));
      if (
        null != this.scene.getObjectByName(this.modele).children[0].material.map
      )
        for (
          h = 0, c = this.scene.getObjectByName(this.modele).children.length;
          h < c;

        )
          delete this.scene.getObjectByName(this.modele).children[h].material
            .map,
            delete this.scene.getObjectByName(this.modele).children[h].material
              .alphaMap,
            this.scene
              .getObjectByName(this.modele)
              .children[h].material.dispose(),
            (this.scene.getObjectByName(this.modele).children[
              h
            ].material.map = F),
            (this.scene.getObjectByName(this.modele).children[
              h
            ].material.alphaMap = g ? w : ""),
            ++h;
      else {
        g = new THREE.MeshBasicMaterial({
          map: F,
          alphaMap: g ? w : "",
          side: THREE.DoubleSide,
          transparent: !0,
          alphaTest: 0.8,
          depthWrite: !0,
          depthTest: !0,
          shading: THREE.FlatShading
        });

        h = 0;
        for (
          c = this.scene.getObjectByName(this.modele).children.length;
          h < c;

        )
          (this.scene.getObjectByName(this.modele).children[h].material = g),
            ++h;

        this.modeleGroup = new THREE.Group();
        //this.modeleGroup.name = this.modele+"_Group";
        this.modeleGroup.rotation.y = I;
        this.modeleGroup.add(this.scene.getObjectByName(this.modele));
        this.modeleGroup.add(
          this.scene.getObjectByName(this.modele + "_bouton")
        );
        this.scene.add(this.modeleGroup);
      }
      "costume" == this.modele &&
        ((C.width = 2048),
        (C.height = 2048),
        C.getContext("2d").drawImage(
          a.data[f - 1][2].image,
          0,
          0,
          a.data[f - 1][2].image.width,
          a.data[f - 1][2].image.height
        ),
        (E.width = a.data[f - 1][4].image.width),
        (E.height = a.data[f - 1][4].image.height),
        (z = E.getContext("2d")),
        z.drawImage(
          a.data[f - 1][6].image,
          0,
          0,
          a.data[f - 1][6].image.width,
          a.data[f - 1][6].image.height
        ),
        (z.globalCompositeOperation = "source-in"),
        z.drawImage(
          a.data[f - 1][4].image,
          0,
          0,
          a.data[f - 1][4].image.width,
          a.data[f - 1][4].image.height
        ),
        C.getContext("2d").drawImage(
          E,
          0,
          535,
          a.data[f - 1][4].image.width,
          a.data[f - 1][4].image.height
        ),
        (g = new THREE.Texture(C)),
        (g.anisotropy = B),
        (g.minFilter = THREE.LinearMipMapLinearFilter),
        (g.magFilter = THREE.LinearFilter),
        (g.wrapS = THREE.ClampToEdgeWrapping),
        (g.wrapT = THREE.ClampToEdgeWrapping),
        (g.needsUpdate = !0),
        (f = new THREE.Texture(a.data[f - 1][3].image)),
        (f.anisotropy = B),
        (f.minFilter = THREE.LinearMipMapLinearFilter),
        (f.magFilter = THREE.LinearFilter),
        (f.wrapS = THREE.ClampToEdgeWrapping),
        (f.wrapT = THREE.ClampToEdgeWrapping),
        (f.needsUpdate = !0),
        null == this.scene.getObjectByName("pantalon").children[0].material.map
          ? ((this.scene.getObjectByName(
              "pantalon"
            ).children[0].material = new THREE.MeshBasicMaterial({
              map: g,
              alphaMap: f,
              side: THREE.DoubleSide,
              transparent: !0,
              alphaTest: 0.5,
              depthWrite: !0,
              depthTest: !0,
              shading: THREE.FlatShading
            })),
            (this.scene.getObjectByName("pantalon").visible = !0),
            this.modeleGroup.add(this.scene.getObjectByName("pantalon")))
          : (delete this.scene.getObjectByName("pantalon").children[0].material
              .map,
            delete this.scene.getObjectByName("pantalon").children[0].material
              .alphaMap,
            this.scene
              .getObjectByName("pantalon")
              .children[0].material.dispose(),
            (this.scene.getObjectByName(
              "pantalon"
            ).children[0].material.map = g),
            (this.scene.getObjectByName(
              "pantalon"
            ).children[0].material.alphaMap = f)),
        "undefined" == typeof this.scene.getObjectByName("ground") &&
          ((f = new Loader3D("pantalon_bouton", this.scene, e)),
          (f.onLoaded = () => {
            new THREE.TextureLoader().load("static/Textures/om.jpg", b => {
              b = new THREE.MeshBasicMaterial({
                color: 0,
                alphaMap: b,
                side: THREE.FrontSide,
                transparent: !0
              });
              this.scene.getObjectByName("pantalon_bouton").visible = !0;
              this.scene.getObjectByName("ground").visible = !0;
              this.scene.getObjectByName("ground").material = b;
              this.modeleGroup.add(
                this.scene.getObjectByName("pantalon_bouton")
              );
            });
          })));
      "pantalon" == this.modele &&
        null == this.scene.getObjectByName("ground").material.alphaMap &&
        ((this.scene.getObjectByName("ground").visible = !1),
        new THREE.TextureLoader().load("static/Textures/om.jpg", a => {
          a = new THREE.MeshBasicMaterial({
            color: 0,
            alphaMap: a,
            side: THREE.FrontSide,
            transparent: !0
          });
          this.scene.getObjectByName("ground").visible = !0;
          this.scene.getObjectByName("ground").material = a;
        }));
      this.scene.getObjectByName(this.modele).visible = !0;
      if (b) {
        v = new THREE.Texture(b);
        v.anisotropy = B;
        v.minFilter = THREE.LinearMipMapLinearFilter;
        v.magFilter = THREE.LinearFilter;
        v.wrapS = THREE.ClampToEdgeWrapping;
        v.wrapT = THREE.ClampToEdgeWrapping;
        v.needsUpdate = !0;
        f = 0;
        for (
          h = this.scene.getObjectByName(this.modele + "_bouton").children
            .length;
          f < h;

        )
          "ground" !=
            this.scene.getObjectByName(this.modele + "_bouton").children[f]
              .name &&
            (null !=
            this.scene.getObjectByName(this.modele + "_bouton").children[f]
              .material.map
              ? (this.scene.getObjectByName(this.modele + "_bouton").children[
                  f
                ].material.map = v)
              : (this.scene.getObjectByName(this.modele + "_bouton").children[
                  f
                ].material = new THREE.MeshBasicMaterial({
                  map: v
                }))),
            ++f;
        b = null;
        this.scene.getObjectByName(this.modele + "_bouton").visible = !0;
      }
      for (h = a.data.length; h--; )
        for (f = a.data[h].length; f--; )
          (b = a.data[h][f]),
            "object" === typeof b &&
              "function" === typeof b.destroy &&
              b.destroy();
    };
    this.order = function() {
      this.data.sort(function(a, b) {
        return a[5] < b[5] ? -1 : a[5] > b[5] ? 1 : 0;
      });
    };
    this.onLoaded = function() {
      g == m && (e.loaded(), a.display(), (m = g = 0));
    };
    this.progressLoader = function() {
      for (var a = 0, b = c.length, g = 0, h = 0; a < b; )
        "undefined" != typeof c[a] &&
          "undefined" != typeof c[a][0] &&
          ((g += c[a][0]), (h += c[a][1])),
          ++a;
      e.onProgress({
        loaded: g,
        total: h
      });
    };
    this.destroyTextureLoading = function() {
      for (var a = 0, b = c.length; a < b; )
        "undefined" != typeof c[a] && c[a][2].abort(), ++a;
    };
    this.destroy = function() {
      if (g == m) {
        //TWEEN.removeAll();
        this.clearMemoryGroup();
        this.modeleGroup = null;
      } else {
        TWEEN.removeAll();
        this.clearMemoryGroup();
        this.modeleGroup = null;
        // for (var a = 0, b = c.length; a < b;) {
        //        if ("undefined" != typeof c[a]) {
        //               c[a][2].abort();
        //               delete c[a][2]
        //        }++a
        // }
      }
    };

    this.clearMemoryGroup = function() {
      for (var a = this.scene.children.length; a--; ) {
        for (var b = this.scene.children[a].children.length; b--; ) {
          var c = this.scene.children[a].children[b];
          c instanceof THREE.Mesh &&
            (delete c.material.map,
            delete c.material.alphaMap,
            c.geometry.dispose(),
            (c.geometry = null),
            c.material.dispose(),
            (c.material = null));
          for (
            var g = this.scene.children[a].children[b].children.length;
            g--;

          )
            (c = this.scene.children[a].children[b].children[g]),
              c instanceof THREE.Mesh &&
                (delete c.material.map,
                delete c.material.alphaMap,
                c.geometry.dispose(),
                (c.geometry = null),
                c.material.dispose(),
                (c.material = null));
        }
        this.scene.remove(this.scene.children[a]);
      }
      a = 0;
      // for (var j = this.data.length; a <j;) {
      //        c = 0;
      //        for (var b = this.data[a].length; c < b;) g = this.data[a][c], "object" === typeof g && c++;
      //        a++;

      // }
    };
    (this.p = function() {
      this.r();

      for (
        var b = this[a.modele + "Grid"],
          e = 0,
          h = a.data.length,
          k = b.length,
          l = 0;
        e < h;

      ) {
        for (l = 0; l < k; ) {
          if (b[l][12] == a.data[e][0]) {
            if (b[l][1]) {
              var path = "media/textures/" +
                  a.modele +
                  "/" +
                  b[l][1]+'_'+
                  a.data[e][1] +
                  ".jpg"
              console.log("path : ", path);
              
              var f = new LoaderTexture(path);
              
              (function(b) {
                c[b] = [l.loaded, l.total, f];
                f.onProgress = function(f) {
                  c[b] = [f.loaded, f.total, this];
                  a.progressLoader();
                };
              })(m);
              (function(c, e) {
                f.onLoaded = function() {
                  g++;

                  

                  a.data[c][2] = this;
                  a.data[c][3] = b[e][1];
                  a.data[c][5] = b[e][5];
                  a.data[c][6] = b[e][6];
                  a.data[c][7] = b[e][9];
                  a.data[c][8] = b[e][11];
                  a.onLoaded();
                };
              })(e, l);
              m++;
            }
            if (b[l][2]) {
              var p = new LoaderTexture(
                "static/Textures/mask/" + b[l][2] + ".png"
              );
              (function(b) {
                c[b] = [l.loaded, l.total, p];
                p.onProgress = function(f) {
                  c[b] = [f.loaded, f.total, this];
                  a.progressLoader();
                };
              })(m);
              (function(b) {
                p.onLoaded = function() {
                  a.data[b][4] = this;
                  g++;
                  a.onLoaded();
                };
              })(e);
              m++;
            }
            if (b[l][10]) {
              var n = new LoaderTexture(
                "static/Textures/alpha/" + b[l][10] + ".jpg"
              );
              (function(b) {
                c[b] = [l.loaded, l.total, n];
                n.onProgress = function(f) {
                  c[b] = [f.loaded, f.total, this];
                  a.progressLoader();
                };
              })(m);
              (function(c, f) {
                n.onLoaded = function() {
                  a.data[c][7] = b[f][9];
                  a.data[c][9] = this;
                  g++;
                  a.onLoaded();
                };
              })(e, l);
              m++;
            }
            !b[l][1] &&
              b[l][11] &&
              ((a.data[e][7] = b[l][9]), (a.data[e][8] = b[l][11]));
          }
          ++l;
        }
        ++e;
      }
      "costume" == a.modele &&
        ((a.data[e] = []),
        (a.data[e][0] = "pantalon"),
        (a.data[e][1] = a.data[0][1]),
        (a.data[e][5] = 99),
        (n = new LoaderTexture(
          "static/Textures/pantalon/pantalon_" + a.data[0][1] + ".jpg"
        )),
        (function(b) {
          c[b] = [l.loaded, l.total, n];
          n.onProgress = function(f) {
            c[b] = [f.loaded, f.total, this];
            a.progressLoader();
          };
        })(m),
        (function(b, c) {
          n.onLoaded = function() {
            a.data[b][2] = this;
            g++;
            a.onLoaded();
          };
        })(e, l),
        m++,
        (n = new LoaderTexture("static/Textures/alpha/pa_pantalon.jpg")),
        (function(b) {
          c[b] = [l.loaded, l.total, n];
          n.onProgress = function(f) {
            c[b] = [f.loaded, f.total, this];
            a.progressLoader();
          };
        })(m),
        (function(b, c) {
          n.onLoaded = function() {
            a.data[b][3] = this;
            g++;
            a.onLoaded();
          };
        })(e, l),
        m++,
        (n = new LoaderTexture("static/Textures/pantalon/p_ombreveste.jpg")),
        (function(b) {
          c[b] = [l.loaded, l.total, n];
          n.onProgress = function(f) {
            c[b] = [f.loaded, f.total, this];
            a.progressLoader();
          };
        })(m),
        (function(b, c) {
          n.onLoaded = function() {
            a.data[b][4] = this;
            g++;
            a.onLoaded();
          };
        })(e, l),
        m++,
        (n = new LoaderTexture("static/Textures/mask/pm_ombreveste.png")),
        (function(b) {
          c[b] = [l.loaded, l.total, n];
          n.onProgress = function(f) {
            c[b] = [f.loaded, f.total, this];
            a.progressLoader();
          };
        })(m),
        (function(b, c) {
          n.onLoaded = function() {
            a.data[b][6] = this;
            g++;
            a.onLoaded();
          };
        })(e, l),
        m++);
    }),
      (this.r = function() {
        for (
          var b = this[a.modele + "Grid"],
            c = a.data.length,
            e = !1,
            g = !1,
            h = !1;
          c--;

        ) {
          for (var e = !1, f = b.length; f--; )
            if (b[f][12] == a.data[c][0]) {
              e = !0;
              if (
                "french_cuff_beveled" == a.data[c][0] ||
                "french_cuff_rounded" == a.data[c][0] ||
                "french_cuff_straight" == a.data[c][0]
              )
                g = a.data[c][1];
              "veste" != this.modele ||
                ("small_large_notched_reverse" != a.data[c][0] &&
                  "small_notched_reverse" != a.data[c][0] &&
                  "small_thin_notched_reverse" != a.data[c][0] &&
                  "small_thin_peaked_reverse" != a.data[c][0] &&
                  "small_peaked_reverse" != a.data[c][0] &&
                  "small_large_peaked_reverse" != a.data[c][0] &&
                  "small_large_shawl" != a.data[c][0] &&
                  "small_shawl" != a.data[c][0] &&
                  "small_thin_shawl" != a.data[c][0]) ||
                (h = a.data[c][1]);
            }
          e || a.data.splice(c, 1);
        }
        g && a.data.push(["french_cuff", g]);
        h && a.data.push(["3_buttons_contrast", h]);
      });
    (b => {
      var e = new Loader3D(a.modele, this.scene);
      e.onLoaded = () => {
        g++;
        a.onLoaded();
        e.destroy();
        e = null;
      };
      (b => {
        c[b] = [e.loaded, e.total, e];
        e.onProgress = function(f) {
          c[b] = [f.loaded, f.total, this];
          a.progressLoader();
        };
      })(m);
      m++;
      var h = new Loader3D(a.modele + "_bouton", this.scene);
      h.onLoaded = function() {
        g++;
        a.onLoaded();
        h.destroy();
        h = null;
      };
      (function(b) {
        c[b] = [h.loaded, h.total, h];
        h.onProgress = function(f) {
          c[b] = [f.loaded, f.total, this];
          a.progressLoader();
        };
      })(m);
      m++;
      if ("costume" == a.modele) {
        var k = new Loader3D("pantalon", this.scene);
        k.onLoaded = function() {
          g++;
          a.onLoaded();
          k.destroy();
          k = null;
        };
        (function(b) {
          c[b] = [k.loaded, k.total, k];
          k.onProgress = function(f) {
            c[b] = [f.loaded, f.total, this];
            a.progressLoader();
          };
        })(m);
        m++;
      }
    })();
    this.p();
  }

  starter_veste = [
    ["fabric", "HS172050"],
    ["jacket_button", "BS003"],
    ["2_buttons", "HS172050"]
  ];

  //data autoselected chemise
  starter_chemise = [["fabric", "HS172050"]];
  /*************************************************** */
  //data autoselected gilet
  starter_gilet = [["fabric", "HS172050"]];
  starter_pantalon = [["fabric", "HS172050"]];
  /*************************************************** */

  giletGrid = [
    [
      "",
      "g_gilet",
      "",
      "gmg_gilet",
      "",
      1,
      [
        {
          x: 0,
          y: 1e3
        },
        {
          x: 0,
          y: 0
        },
        {
          x: 836,
          y: 456
        }
      ],
      [
        {
          w: 1e3,
          h: 1300
        },
        {
          w: 1e3,
          h: 999
        },
        {
          w: 1040,
          h: 1128
        }
      ],
      "",
      "gilet",
      "ga_gilet",
      "",
      "fabric",
      ""
    ],
    [
      "",
      "g_col5",
      "",
      "gmg_col5",
      "",
      3,
      {
        x: 240,
        y: 1116
      },
      "",
      "",
      "gilet",
      "",
      "bouton5",
      "5_buttons",
      ""
    ],
    [
      "",
      "g_col6",
      "",
      "gmg_col6",
      "",
      3,
      {
        x: 196,
        y: 1044
      },
      "",
      "",
      "gilet",
      "",
      "bouton6",
      "6_buttons",
      ""
    ],
    [
      "",
      "g_col5rond",
      "",
      "gmg_col5rond",
      "",
      3,
      {
        x: 222,
        y: 1112
      },
      "",
      "",
      "gilet",
      "",
      "bouton5",
      "5_rounded_buttons",
      ""
    ],
    [
      "",
      "g_bouton",
      "",
      "",
      "1",
      1,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "bouton",
      "",
      "",
      "vest_button",
      ""
    ],
    [
      "",
      "g_gilet",
      "gm_doublure",
      "",
      "1",
      2,
      {
        x: 0,
        y: 0
      },
      {
        w: 924,
        h: 1030
      },
      "",
      "gilet",
      "",
      "",
      "vest_lining",
      ""
    ],
    [
      "",
      "g_pochePoitrine",
      "gm_pochePoitrine",
      "gmg_pochePoitrine",
      "",
      4,
      {
        x: 506,
        y: 1384
      },
      "",
      "",
      "gilet",
      "",
      "",
      "vest_pocket_breast",
      ""
    ],
    [
      "",
      "g_pocheApplique",
      "",
      "",
      "",
      5,
      {
        x: 119,
        y: 1724
      },
      "",
      "",
      "gilet",
      "",
      "",
      "vest_pocket_apply",
      ""
    ],
    [
      "",
      "g_poche_droite",
      "gm_poche_droite",
      "gmg_poche_droite",
      "",
      5,
      {
        x: 103,
        y: 1751
      },
      "",
      105,
      "gilet",
      "",
      "",
      "vest_pocket_left",
      ""
    ],
    [
      "",
      "g_poche_gauche",
      "gm_poche_gauche",
      "gmg_poche_gauche",
      "",
      5,
      {
        x: 502,
        y: 1751
      },
      "",
      80,
      "gilet",
      "",
      "",
      "vest_pocket_right",
      ""
    ]
  ];
  pantalonGrid = [
    [
      "",
      "p_pantalon",
      "",
      "pmg_pantalon",
      "",
      1,
      [
        {
          x: 0,
          y: 500
        },
        {
          x: 840,
          y: 500
        },
        {
          x: 734,
          y: 248
        },
        {
          x: 588,
          y: 68
        },
        {
          x: 1790,
          y: 148
        }
      ],
      [
        {
          w: 800,
          h: 2500
        },
        {
          w: 900,
          h: 2500
        },
        {
          w: 584,
          h: 184
        },
        {
          w: 974,
          h: 100
        },
        {
          w: 224,
          h: 224
        }
      ],
      "",
      "pantalon",
      "pa_pantalon",
      "",
      "fabric"
    ],
    [
      "",
      "p_pince",
      "pm_pince2",
      "",
      "",
      3,
      {
        x: 597,
        y: 578
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "m2"
    ],
    [
      "",
      "p_pince",
      "",
      "",
      "1",
      3,
      {
        x: 597,
        y: 578
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "m4"
    ],
    [
      "",
      "p_ceinture_courtelongue",
      "pm_ceinture_longue",
      "",
      "",
      4,
      {
        x: 679,
        y: 520
      },
      "",
      90,
      "pantalon",
      "",
      "boutonCeintureLongue",
      "pants_belt_long"
    ],
    [
      "",
      "p_ceinture_courtelongue",
      "pm_ceinture_courte",
      "",
      "1",
      4,
      {
        x: 679,
        y: 520
      },
      "",
      90,
      "pantalon",
      "",
      "boutonCeintureCourte",
      "pants_belt_short"
    ],
    [
      "",
      "p_pocheItalie",
      "pm_pocheItalie",
      "",
      "",
      5,
      {
        x: 395,
        y: 574
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "italian"
    ],
    [
      "",
      "p_pocheCouture",
      "pm_pocheCouture",
      "",
      "",
      5,
      {
        x: 395,
        y: 574
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "sewings"
    ],
    [
      "",
      "p_pocheCavaliere",
      "pm_pocheCavaliere",
      "",
      "",
      5,
      {
        x: 433,
        y: 585
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "western"
    ],
    [
      "",
      "p_pocheArriereRabat",
      "",
      "",
      "",
      6,
      {
        x: 737,
        y: 301
      },
      "",
      [-10, 5],
      "pantalon",
      "pa_pocheArriereRabatDroite",
      "boutonPocheDroite",
      "pants_back_pocket_right"
    ],
    [
      "",
      "p_pocheArriereRabat",
      "",
      "",
      "1",
      7,
      {
        x: 737,
        y: 301
      },
      "",
      "",
      "pantalon",
      "pa_pocheArriereRabatGauche",
      "boutonPocheGauche",
      "pants_back_pocket_left"
    ],
    [
      "",
      "p_pocheArrierePassepoil",
      "pm_pocheArrierePassepoilDroite",
      "",
      "",
      6,
      {
        x: 140,
        y: 627
      },
      "",
      "",
      "pantalon",
      "",
      "boutonPochePassDroite",
      "pants_back_pocket_pipping_right"
    ],
    [
      "",
      "p_pocheArrierePassepoil",
      "pm_pocheArrierePassepoilGauche",
      "",
      "1",
      7,
      {
        x: 140,
        y: 627
      },
      "",
      "",
      "pantalon",
      "",
      "boutonPochePassGauche",
      "pants_back_pocket_pipping_left"
    ],
    [
      "",
      "p_ourlet",
      "pm_ourlet",
      "",
      "",
      9,
      {
        x: 140,
        y: 1909
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "pants_flap"
    ],
    [
      "",
      "p_pattesSerrage",
      "",
      "pmg_pattesSerrage",
      "",
      10,
      {
        x: 1230,
        y: 69
      },
      "",
      "",
      "pantalon",
      "pa_pattesSerrage",
      "",
      "tightening_strap"
    ],
    [
      "",
      "p_passants",
      "pm_6passants",
      "",
      "",
      11,
      {
        x: 127,
        y: 509
      },
      "",
      "",
      "pantalon",
      "pa_6passants",
      "",
      "loop_2"
    ],
    [
      "",
      "p_passants",
      "pm_4passants",
      "",
      "",
      11,
      {
        x: 127,
        y: 509
      },
      "",
      "",
      "pantalon",
      "pa_4passants",
      "",
      "with_belt_loop"
    ],
    [
      "",
      "",
      "",
      "",
      "1",
      12,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "pantalon",
      "pa_ardillon",
      "",
      "pants_pin_buckle"
    ],
    [
      "",
      "p_ceinture",
      "pm_ceinture",
      "",
      "",
      2,
      {
        x: 48,
        y: 496
      },
      "",
      90,
      "pantalon",
      "",
      "",
      "belt"
    ],
    [
      "",
      "p_pantalon",
      "pm_bandeSatin",
      "",
      "1",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "strip"
    ],
    [
      "",
      "p_bouton",
      "",
      "",
      "1",
      1,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "bouton",
      "",
      "",
      "pants_button"
    ],
    [
      "",
      "p_ombreveste",
      "pm_ombreveste",
      "",
      "1",
      13,
      {
        x: 0,
        y: 535
      },
      "",
      "",
      "pantalon",
      "",
      "",
      "shadow_suit"
    ]
  ];
  chemiseGrid = [
    [
      "",
      "c_chemise",
      "",
      "",
      "",
      1,
      [
        {
          x: 0,
          y: 750
        },
        {
          x: 0,
          y: 0
        },
        {
          x: 770,
          y: 0
        },
        {
          x: 1300,
          y: 0
        },
        {
          x: 1683,
          y: 930
        },
        {
          x: 1539,
          y: 1163
        },
        {
          x: 1587,
          y: 1647
        },
        {
          x: 771,
          y: 912
        },
        {
          x: 1496,
          y: 977
        }
      ],
      [
        {
          w: 800,
          h: 1500
        },
        {
          w: 766,
          h: 750
        },
        {
          w: 550,
          h: 920
        },
        {
          w: 580,
          h: 940
        },
        {
          w: 365,
          h: 244
        },
        {
          w: 374,
          h: 240
        },
        {
          w: 461,
          h: 401
        },
        {
          w: 768,
          h: 1125
        },
        {
          w: 170,
          h: 165
        }
      ],
      "",
      "chemise",
      "ca_basChemiseRond",
      "",
      "fabric"
    ],
    [
      "",
      "c_col_long",
      "cm_col_long",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_long",
      "boutonCol",
      "long"
    ],
    [
      "",
      "c_col_napolitain",
      "cm_col_napolitain",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_napolitain",
      "boutonCol",
      "napolitan"
    ],
    [
      "",
      "c_col_italien",
      "cm_col_italien",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_italien",
      "boutonCol",
      "classic"
    ],
    [
      "",
      "c_col_italien",
      "cm_col_italien",
      "cmg_col",
      "1",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_italien",
      "boutonColClassique2",
      "classic_2_button"
    ],
    [
      "",
      "c_col_fin",
      "cm_col_fin",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_fin",
      "boutonCol",
      "thin"
    ],
    [
      "",
      "c_col_ultrafin",
      "cm_col_ultrafin",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_ultrafin",
      "boutonCol",
      "very_thin"
    ],
    [
      "",
      "c_col_claudine",
      "cm_col_claudine",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_claudine",
      "boutonCol",
      "clodine"
    ],
    [
      "",
      "c_col_claudinefin",
      "cm_col_claudinefin",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_claudinefin",
      "boutonCol",
      "clodine_thin"
    ],
    [
      "",
      "c_col_claudineultrafin",
      "cm_col_claudineultrafin",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_claudineultrafin",
      "boutonCol",
      "clodine_very_thin"
    ],
    [
      "",
      "c_col_claudinenapoli",
      "cm_col_claudinenapoli",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_claudinenapoli",
      "boutonCol",
      "napolitan_clodine"
    ],
    [
      "",
      "",
      "",
      "cmg_col",
      "1",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_officier",
      "boutonCol",
      "stand_up"
    ],
    [
      "",
      "c_col_classique",
      "cm_col_classique",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_classique",
      "boutonCol",
      "italian"
    ],
    [
      "",
      "c_col_casse",
      "cm_col_casse",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_casse",
      "boutonCol",
      "wing"
    ],
    [
      "",
      "c_col_france",
      "cm_col_france",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_france",
      "boutonCol",
      "french"
    ],
    [
      "",
      "c_col_anglais",
      "cm_col_anglais",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_anglais",
      "boutonColanglais",
      "english"
    ],
    [
      "",
      "c_col_americain",
      "cm_col_classique",
      "cmg_col",
      "",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_classique",
      "boutonColAmericain",
      "american"
    ],
    [
      "",
      "c_col_americain",
      "cm_col_classique",
      "cmg_col",
      "1",
      8,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_col_classique",
      "boutonColAmericain",
      "american_thin"
    ],
    [
      "",
      "c_biseaute",
      "",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet1",
      "bevel_1"
    ],
    [
      "",
      "c_biseaute2",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet2",
      "bevel_2"
    ],
    [
      "",
      "c_biseauteAjustable",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet3",
      "adjustable_beveled"
    ],
    [
      "",
      "c_droit",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet1",
      "straight_1"
    ],
    [
      "",
      "c_droit2",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet2",
      "straight_2"
    ],
    [
      "",
      "c_droitAjustable",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet3",
      "ajustable_straight"
    ],
    [
      "",
      "c_rond",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet1",
      "rounded_1"
    ],
    [
      "",
      "c_rond2",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet2",
      "rounded_2"
    ],
    [
      "",
      "c_rondAjustable",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet3",
      "ajustable_rounded"
    ],
    [
      "",
      "c_napolitain",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet1",
      "napolitan_1"
    ],
    [
      "",
      "c_napolitain2",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoignet2",
      "napolitan_2"
    ],
    [
      "",
      "c_droit",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "ca_fin",
      "boutonPoignetthin",
      "thin_poignet"
    ],
    [
      "",
      "c_droitAjustable",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "ca_fin",
      "boutonPoignetthin2",
      "ajustable_thin"
    ],
    [
      "",
      "c_mousquetaireBiseaute",
      "cm_Mousquetaire",
      "",
      "",
      3,
      {
        x: 1539,
        y: 953
      },
      "",
      "",
      "chemise",
      "ca_mousquetaire",
      "boutonMousqu",
      "french_cuff_beveled"
    ],
    [
      "",
      "c_mousquetaireRond",
      "cm_Mousquetaire",
      "",
      "",
      3,
      {
        x: 1539,
        y: 953
      },
      "",
      "",
      "chemise",
      "ca_mousquetaire",
      "boutonMousqu",
      "french_cuff_rounded"
    ],
    [
      "",
      "c_mousquetaireDroit",
      "cm_Mousquetaire",
      "",
      "",
      3,
      {
        x: 1539,
        y: 953
      },
      "",
      "",
      "chemise",
      "ca_mousquetaire",
      "boutonMousqu",
      "french_cuff_straight"
    ],
    [
      "",
      "c_galon",
      "",
      "",
      "",
      15,
      {
        x: 1587,
        y: 1647
      },
      "",
      "",
      "chemise",
      "ca_galon",
      "boutonGalon",
      "shirt_braid"
    ],
    [
      "",
      "c_poche_western",
      "cm_poche_western",
      "",
      "",
      4,
      {
        x: 926,
        y: 1217
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoche",
      "western_1"
    ],
    [
      "",
      "c_poche_western",
      "cm_poche_western2",
      "",
      "",
      5,
      {
        x: 926,
        y: 1217
      },
      "",
      "",
      "chemise",
      "",
      "boutonPoche2",
      "western_2"
    ],
    [
      "",
      "c_poche_diamant",
      "cm_poche_diamant",
      "",
      "",
      4,
      {
        x: 1232,
        y: 1220
      },
      "",
      "",
      "chemise",
      "",
      "",
      "diamond"
    ],
    [
      "",
      "c_poche_rond",
      "cm_poche_rond",
      "",
      "",
      4,
      {
        x: 1232,
        y: 1220
      },
      "",
      "",
      "chemise",
      "",
      "",
      "round_edge"
    ],
    [
      "",
      "c_poche_western",
      "cm_poche_western_liseret",
      "",
      "",
      6,
      {
        x: 926,
        y: 1217
      },
      "",
      "",
      "chemise",
      "",
      "",
      "western_1_border"
    ],
    [
      "",
      "c_poche_western",
      "cm_poche_western_liseret2",
      "",
      "",
      7,
      {
        x: 926,
        y: 1217
      },
      "",
      "",
      "chemise",
      "",
      "",
      "western_2_border"
    ],
    [
      "",
      "c_patteBoutonnage",
      "cm_patteBoutonnage",
      "",
      "",
      2,
      {
        x: 108,
        y: 680
      },
      "",
      "",
      "chemise",
      "",
      "patteBoutonnage",
      "simple"
    ],
    [
      "",
      "c_patteBoutonnage_cache",
      "cm_patteBoutonnage",
      "",
      "",
      2,
      {
        x: 108,
        y: 680
      },
      "",
      "",
      "chemise",
      "",
      "",
      "hidden"
    ],
    [
      "",
      "",
      "",
      "",
      "1",
      2,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "",
      "patteBoutonnage",
      "no_placket"
    ],
    [
      "",
      "c_mancheCourte",
      "cm_mancheCourte",
      "",
      "",
      9,
      {
        x: 0,
        y: 1328
      },
      "",
      "",
      "chemise",
      "ca_mancheCourte",
      "",
      "biceps_without_reverse"
    ],
    [
      "",
      "c_mancheCourteRever",
      "cm_mancheCourteRever",
      "",
      "",
      9,
      {
        x: 0,
        y: 330
      },
      "",
      "",
      "chemise",
      "ca_mancheCourte",
      "",
      "biceps_with_reverse"
    ],
    [
      "",
      "c_mancheCourteCoude",
      "cm_mancheCourteCoudeRever",
      "",
      "",
      9,
      {
        x: 0,
        y: 377
      },
      "",
      "",
      "chemise",
      "ca_mancheCourteCoude",
      "",
      "elbow_without_reverse"
    ],
    [
      "",
      "c_mancheCourteCoudeRever",
      "cm_mancheCourteCoudeRever",
      "",
      "",
      9,
      {
        x: 0,
        y: 377
      },
      "",
      "",
      "chemise",
      "ca_mancheCourteCoude",
      "",
      "elbow_with_reverse"
    ],
    [
      "",
      "",
      "",
      "",
      "1",
      10,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "ca_basChemiseDroit",
      "",
      "shirt_bottom_right"
    ],
    [
      "",
      "c_taillesxs",
      "",
      "",
      "",
      11,
      {
        x: 183,
        y: 1216
      },
      "",
      "",
      "chemise",
      "",
      "",
      "s"
    ],
    [
      "",
      "c_taillesxs",
      "",
      "",
      "",
      11,
      {
        x: 183,
        y: 1216
      },
      "",
      "",
      "chemise",
      "",
      "",
      "xs"
    ],
    [
      "",
      "c_taillesl2",
      "cm_taillesl2",
      "",
      "",
      11,
      {
        x: 224,
        y: 1053
      },
      "",
      "",
      "chemise",
      "",
      "",
      "l1"
    ],
    [
      "",
      "c_taillesl1",
      "cm_taillesl1",
      "",
      "",
      11,
      {
        x: 224,
        y: 1053
      },
      "",
      "",
      "chemise",
      "",
      "",
      "l2"
    ],
    [
      "",
      "c_bouton",
      "",
      "",
      "1",
      1,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "bouton",
      "",
      "",
      "shirt_button"
    ],
    [
      "",
      "c_chemise",
      "cm_interieurCol",
      "",
      "1",
      12,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "",
      "",
      "shirt_inside_collar_stand_fabric"
    ],
    [
      "",
      "c_chemise",
      "cm_exterieurCol",
      "",
      "1",
      13,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "",
      "",
      "shirt_collar_stand_fabric"
    ],
    [
      "",
      "c_chemise",
      "cm_arriereChemise",
      "",
      "1",
      14,
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "chemise",
      "",
      "",
      "shirt_back_yoke_fabric"
    ],
    [
      "",
      "c_rond",
      "cm_rond",
      "",
      "",
      3,
      {
        x: 837,
        y: 779
      },
      "",
      "",
      "chemise",
      "",
      "",
      "french_cuff"
    ]
  ];
  vesteGrid = [
    [
      "veste",
      "v_veste",
      "",
      "vmg_veste",
      "",
      "1",
      [
        {
          x: 0,
          y: 782
        },
        {
          x: 856,
          y: 816
        },
        {
          x: 1352,
          y: 66
        },
        {
          x: 1526,
          y: 1070
        }
      ],
      [
        {
          w: 864,
          h: 1266
        },
        {
          w: 758,
          h: 1232
        },
        {
          w: 566,
          h: 964
        },
        {
          w: 522,
          h: 978
        }
      ],
      "",
      "veste",
      "",
      "",
      "fabric"
    ],
    [
      "col Descendant Large",
      "v_colDescendant_large",
      "vm_colDescendant_large",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "large_notched_collar"
    ],
    [
      "col Descendant",
      "v_colDescendant",
      "vm_colDescendant",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "notched_collar"
    ],
    [
      "col Descendant fin",
      "v_colDescendant_fin",
      "vm_colDescendant_fin",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "thin_notched_collar"
    ],
    [
      "revers Descendant Large",
      "v_reversDescendant_large",
      "vm_reversDescendant_large",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "large_notched_reverse"
    ],
    [
      "revers Descendant",
      "v_reversDescendant",
      "vm_reversDescendant",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "notched_reverse"
    ],
    [
      "revers Descendant fin",
      "v_reversDescendant_fin",
      "vm_reversDescendant_fin",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "thin_notched_reverse"
    ],
    [
      "col Descendant Large liseret",
      "v_colDescendant_large",
      "vm_colDescendant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "large_notched_collar_border"
    ],
    [
      "col Descendant liseret",
      "v_colDescendant",
      "vm_colDescendant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "notched_collar_border"
    ],
    [
      "col Descendant fin liseret",
      "v_colDescendant_fin",
      "vm_colDescendant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "thin_notched_collar_border"
    ],
    [
      "revers Descendant Large liseret",
      "v_reversDescendant_large",
      "vm_reversDescendant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "large_notched_reverse_border"
    ],
    [
      "revers Descendant liseret",
      "v_reversDescendant",
      "vm_reversDescendant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "notched_reverse_border"
    ],
    [
      "revers Descendant fin liseret",
      "v_reversDescendant_fin",
      "vm_reversDescendant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "thin_notched_reverse_border"
    ],
    [
      "col Montant Large",
      "v_colMontant_large",
      "vm_colMontant_large",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "large_peaked_collar"
    ],
    [
      "col Montant ",
      "v_colMontant",
      "vm_colMontant",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "peaked_collar"
    ],
    [
      "col Montant fin",
      "v_colMontant_fin",
      "vm_colMontant_fin",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [30, -30],
      "veste",
      "",
      "",
      "thin_peaked_collar"
    ],
    [
      "revers Montant Large",
      "v_reversMontant_large",
      "vm_reversMontant_large",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "large_peaked_reverse"
    ],
    [
      "revers Montant ",
      "v_reversMontant",
      "vm_reversMontant",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "peaked_reverse"
    ],
    [
      "revers Montant fin",
      "v_reversMontant_fin",
      "vm_reversMontant_fin",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "thin_peaked_reverse"
    ],
    [
      "col Montant Large liseret",
      "v_colMontant_large",
      "vm_colMontant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "large_peaked_collar_border"
    ],
    [
      "col Montant liseret",
      "v_colMontant",
      "vm_colMontant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "peaked_collar_border"
    ],
    [
      "col Montant fin liseret",
      "v_colMontant_fin",
      "vm_colMontant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "thin_peaked_collar_border"
    ],
    [
      "revers Montant Large liseret",
      "v_reversMontant_large",
      "vm_reversMontant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "large_peaked_reverse_border"
    ],
    [
      "revers Montant liseret",
      "v_reversMontant",
      "vm_reversMontant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "peaked_reverse_border"
    ],
    [
      "revers Montant fin liseret",
      "v_reversMontant_fin",
      "vm_reversMontant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "thin_peaked_reverse_border"
    ],
    [
      "col Revers Chale Large",
      "v_colChale_large",
      "vm_colChale_large",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "large_shawl"
    ],
    [
      "col Revers Chale",
      "v_colChale",
      "vm_colChale",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "shawl"
    ],
    [
      "col Revers Chale fin",
      "v_colChale_fin",
      "vm_colChale_fin",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "thin_shawl"
    ],
    [
      "col Revers Chale Large liseret",
      "v_colChale_large",
      "vm_colChale_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "large_shawl_border"
    ],
    [
      "col Revers  Chale liseret",
      "v_colChale",
      "vm_colChale_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "shawl_border"
    ],
    [
      "col Revers  Chale fin liseret",
      "v_colChale_fin",
      "vm_colChale_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "thin_shawl_border"
    ],
    [
      "boutonniere col",
      "v_col_boutonniere",
      "vm_col_boutonniere",
      "",
      "",
      "12",
      {
        x: 594,
        y: 1101
      },
      "",
      "",
      "veste",
      "",
      "",
      "jacket_buttonhole_collar_color"
    ],
    [
      "boutonniere manche",
      "v_manche_boutonniere",
      "vm_manche_boutonniere",
      "",
      "",
      "13",
      {
        x: 1654,
        y: 830
      },
      "",
      "",
      "veste",
      "",
      "",
      "jacket_buttonhole_all_sleeve_color"
    ],
    [
      "fente 1",
      "v_fente_1",
      "",
      "",
      "",
      "7",
      {
        x: 1147,
        y: 1657
      },
      "",
      "",
      "veste",
      "",
      "",
      "1_slot"
    ],
    [
      "fente 2",
      "v_fente_2",
      "",
      "",
      "",
      "7",
      {
        x: 948,
        y: 1656
      },
      "",
      "",
      "veste",
      "",
      "",
      "2_slot"
    ],
    [
      "patch",
      "v_patch",
      "vm_patch",
      "",
      "",
      "8",
      {
        x: 1584,
        y: 452
      },
      "",
      "",
      "veste",
      "",
      "",
      "jacket_patch"
    ],
    [
      "poche rabat",
      "v_poche",
      "vm_poche",
      "",
      "",
      "10",
      {
        x: 66,
        y: 1612
      },
      "",
      "",
      "veste",
      "",
      "",
      "flap"
    ],
    [
      "poche passepoil",
      "v_pochePassepoil",
      "",
      "",
      "",
      "10",
      {
        x: 66,
        y: 1612
      },
      "",
      "",
      "veste",
      "",
      "",
      "double_piping"
    ],
    [
      "poche appliqu\u00e9e",
      "v_pocheApplique",
      "",
      "",
      "",
      "10",
      {
        x: 66,
        y: 1612
      },
      "",
      "",
      "veste",
      "",
      "",
      "apply"
    ],
    [
      "poche rabat oblique",
      "v_poche_oblique",
      "vm_poche_oblique",
      "",
      "",
      "10",
      {
        x: 66,
        y: 1611
      },
      "",
      "",
      "veste",
      "",
      "",
      "flap_slanting"
    ],
    [
      "poche passepoil oblique",
      "v_pochePassepoil_oblique",
      "vm_pochePassepoil_oblique",
      "",
      "",
      "10",
      {
        x: 62,
        y: 1607
      },
      "",
      "",
      "veste",
      "",
      "",
      "double_piping_slanting"
    ],
    [
      "ticket poche rabat",
      "v_ticket",
      "vm_ticket",
      "",
      "",
      "9",
      {
        x: 148,
        y: 1540
      },
      "",
      "",
      "veste",
      "",
      "",
      "ticket_flap"
    ],
    [
      "ticket poche passepoil",
      "v_ticketPassepoil",
      "vm_ticketPassepoil",
      "",
      "",
      "9",
      {
        x: 148,
        y: 1540
      },
      "",
      "",
      "veste",
      "",
      "",
      "ticket_piping"
    ],
    [
      "bouton veste",
      "v_bouton",
      "",
      "",
      "1",
      "1",
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "bouton",
      "",
      "",
      "jacket_button"
    ],
    [
      "cravate",
      "v_cravate",
      "vm_cravate",
      "",
      "",
      "11",
      {
        x: 367,
        y: 1017
      },
      "",
      "",
      "veste",
      "",
      "",
      "cravate"
    ],
    [
      "ticket poche rabat oblique",
      "v_ticket_oblique",
      "vm_ticket_oblique",
      "",
      "",
      "9",
      {
        x: 148,
        y: 1540
      },
      "",
      "",
      "veste",
      "",
      "",
      "ticket_flap_slanting"
    ],
    [
      "ticket poche passepoil oblique",
      "v_ticketPassepoil_oblique",
      "vm_ticketPassepoil_oblique",
      "",
      "",
      "9",
      {
        x: 148,
        y: 1540
      },
      "",
      "",
      "veste",
      "",
      "",
      "ticket_piping_slanting"
    ],
    [
      "1 bouton de veste",
      "v_bouton_1",
      "",
      "",
      "",
      "2",
      {
        x: 381,
        y: 1608
      },
      "",
      "",
      "veste",
      "",
      "bouton1",
      "1_button"
    ],
    [
      "2 boutons de veste",
      "",
      "",
      "",
      "1",
      "2",
      {
        x: 0,
        y: 0
      },
      "",
      "",
      "veste",
      "",
      "bouton2",
      "2_buttons"
    ],
    [
      "3 boutons de veste",
      "v_bouton_3",
      "vm_bouton_3",
      "",
      "",
      "2",
      {
        x: 203,
        y: 969
      },
      "",
      "",
      "veste",
      "",
      ["bouton3", "colHaut"],
      "3_buttons"
    ],
    [
      "3 boutons contraste revers",
      "v_bouton_3",
      "vm_bouton_3_contraste",
      "",
      "1",
      "2",
      {
        x: 203,
        y: 969
      },
      "",
      "",
      "veste",
      "",
      "",
      "3_buttons_contrast"
    ],
    [
      "2 boutons de veste crois\u00e9e",
      "v_cross_2",
      "vm_cross_2",
      "",
      "",
      "2",
      {
        x: 205,
        y: 932
      },
      "",
      "",
      "veste",
      "",
      ["cross2", "colHaut", "croise"],
      "cross_2_buttons"
    ],
    [
      "4 boutons de veste crois\u00e9e",
      "v_cross_4",
      "vm_cross_2",
      "",
      "",
      "2",
      {
        x: 205,
        y: 932
      },
      "",
      "",
      "veste",
      "",
      ["cross4", "colHaut", "croise"],
      "cross_4_buttons"
    ],
    [
      "boutonniere col fin",
      "v_col_boutonniere",
      "vm_col_boutonniere",
      "",
      "1",
      "12",
      {
        x: 580,
        y: 1101
      },
      "",
      "",
      "veste",
      "",
      "",
      "jacket_buttonhole_collar_color_thin"
    ],
    [
      "boutonniere col large",
      "v_col_boutonniere",
      "vm_col_boutonniere",
      "",
      "1",
      "12",
      {
        x: 605,
        y: 1101
      },
      "",
      "",
      "veste",
      "",
      "",
      "jacket_buttonhole_collar_color_large"
    ],
    [
      "petit revers Descendant Large",
      "v_reversDescendant_large",
      "vm_reversDescendant_large",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_large_notched_reverse"
    ],
    [
      "petit revers Descendant",
      "v_reversDescendant",
      "vm_reversDescendant",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_notched_reverse"
    ],
    [
      "petit revers Descendant fin",
      "v_reversDescendant_fin",
      "vm_reversDescendant_fin",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_thin_notched_reverse"
    ],
    [
      "petit revers Descendant Large liseret",
      "v_reversDescendant_large",
      "vm_reversDescendant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_large_notched_reverse_border"
    ],
    [
      "petit revers Descendant liseret",
      "v_reversDescendant",
      "vm_reversDescendant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_notched_reverse_border"
    ],
    [
      "petit revers Descendant fin liseret",
      "v_reversDescendant_fin",
      "vm_reversDescendant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_thin_notched_reverse_border"
    ],
    [
      "petit revers Montant Large",
      "v_reversMontant_large",
      "vm_reversMontant_large",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_large_peaked_reverse"
    ],
    [
      "petit revers Montant ",
      "v_reversMontant",
      "vm_reversMontant",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_peaked_reverse"
    ],
    [
      "petit revers Montant fin",
      "v_reversMontant_fin",
      "vm_reversMontant_fin",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_thin_peaked_reverse"
    ],
    [
      "petit revers Montant Large liseret",
      "v_reversMontant_large",
      "vm_reversMontant_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_large_peaked_reverse_border"
    ],
    [
      "petit revers Montant liseret",
      "v_reversMontant",
      "vm_reversMontant_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_peaked_reverse_border"
    ],
    [
      "petit revers Montant fin liseret",
      "v_reversMontant_fin",
      "vm_reversMontant_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_thin_peaked_reverse_border"
    ],
    [
      "revers croise Descendant Large",
      "v_croiseDescendant_large",
      "vm_croiseDescendant_large",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_large_notched_reverse"
    ],
    [
      "revers croise  Descendant",
      "v_croiseDescendant",
      "vm_croiseDescendant",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_notched_reverse"
    ],
    [
      "revers croise  Descendant fin",
      "v_croiseDescendant_fin",
      "vm_croiseDescendant_fin",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_thin_notched_reverse"
    ],
    [
      "revers croise  Descendant Large liseret",
      "v_croiseDescendant_large",
      "vm_croiseDescendant_large_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_notched_reverse_border"
    ],
    [
      "revers croise  Descendant liseret",
      "v_croiseDescendant",
      "vm_croiseDescendant_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_notched_reverse_border"
    ],
    [
      "revers croise  Descendant fin liseret",
      "v_croiseDescendant_fin",
      "vm_croiseDescendant_fin_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_notched_reverse_border"
    ],
    [
      "revers croise  Montant Large",
      "v_croiseMontant_large",
      "vm_croiseMontant_large",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_large_peaked_reverse"
    ],
    [
      "revers croise  Montant ",
      "v_croiseMontant",
      "vm_croiseMontant",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_peaked_reverse"
    ],
    [
      "revers croise  Montant fin",
      "v_croiseMontant_fin",
      "vm_croiseMontant_fin",
      "",
      "",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      [-32, 32],
      "veste",
      "",
      "",
      "cross_thin_peaked_reverse"
    ],
    [
      "revers croise  Montant Large liseret",
      "v_croiseMontant_large",
      "vm_croiseMontant_large_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_peaked_reverse_border"
    ],
    [
      "revers croise  Montant liseret",
      "v_croiseMontant",
      "vm_croiseMontant_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_peaked_reverse_border"
    ],
    [
      "revers croise  Montant fin liseret",
      "v_croiseMontant_fin",
      "vm_croiseMontant_fin_liseret",
      "",
      "1",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_peaked_reverse_border"
    ],
    [
      "col croise Descendant Large",
      "v_colDescendant_large",
      "vm_colDescendant_large",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_notched_collar"
    ],
    [
      "col croise Descendant",
      "v_colDescendant",
      "vm_colDescendant",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_notched_collar"
    ],
    [
      "col croise Descendant fin",
      "v_colDescendant_fin",
      "vm_colDescendant_fin",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_notched_collar"
    ],
    [
      "col croise Descendant Large liseret",
      "v_colDescendant_large",
      "vm_colDescendant_large_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_notched_collar_border"
    ],
    [
      "col croise Descendant liseret",
      "v_colDescendant",
      "vm_colDescendant_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_notched_collar_border"
    ],
    [
      "col croise Descendant fin liseret",
      "v_colDescendant_fin",
      "vm_colDescendant_fin_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_notched_collar_border"
    ],
    [
      "col croise Montant Large",
      "v_colMontant_large",
      "vm_colMontant_large",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_peaked_collar"
    ],
    [
      "col croise Montant ",
      "v_colMontant",
      "vm_colMontant",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_peaked_collar"
    ],
    [
      "col croise Montant fin",
      "v_colMontant_fin",
      "vm_colMontant_fin",
      "",
      "1",
      "3",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_peaked_collar"
    ],
    [
      "col croise Montant Large liseret",
      "v_colMontant_large",
      "vm_colMontant_large_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_peaked_collar_border"
    ],
    [
      "col croise Montant liseret",
      "v_colMontant",
      "vm_colMontant_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_peaked_collar_border"
    ],
    [
      "col croise Montant fin liseret",
      "v_colMontant_fin",
      "vm_colMontant_fin_liseret",
      "",
      "1",
      "4",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_peaked_collar_border"
    ],
    [
      "petit Chale Large",
      "v_petitChale_large",
      "vm_petitChale_large",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "small_large_shawl"
    ],
    [
      "petit Chale",
      "v_petitChale",
      "vm_petitChale",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "small_shawl"
    ],
    [
      "petit Chale fin",
      "v_petitChale_fin",
      "vm_petitChale_fin",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "small_thin_shawl"
    ],
    [
      "petit Chale Large liseret",
      "v_petitChale_large",
      "vm_petitChale_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_large_shawl_border"
    ],
    [
      "petit   Chale liseret",
      "v_petitChale",
      "vm_petitChale_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_shawl_border"
    ],
    [
      "petit   Chale fin liseret",
      "v_petitChale_fin",
      "vm_petitChale_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "small_thin_shawl_border"
    ],
    [
      "croise Chale Large",
      "v_croiseChale_large",
      "vm_croiseChale_large",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "cross_large_shawl"
    ],
    [
      "croise Chale",
      "v_croiseChale",
      "vm_croiseChale",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "cross_shawl"
    ],
    [
      "croise Chale fin",
      "v_croiseChale_fin",
      "vm_croiseChale_fin",
      "",
      "",
      "5",
      {
        x: 149,
        y: 882
      },
      "",
      [-20, 20, -60, 65],
      "veste",
      "",
      "",
      "cross_thin_shawl"
    ],
    [
      "croise Chale Large liseret",
      "v_croiseChale_large",
      "vm_croiseChale_large_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_large_shawl_border"
    ],
    [
      "croise Chale liseret",
      "v_croiseChale",
      "vm_croiseChale_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_shawl_border"
    ],
    [
      "croise Chale fin liseret",
      "v_croiseChale_fin",
      "vm_croiseChale_fin_liseret",
      "",
      "1",
      "6",
      {
        x: 149,
        y: 882
      },
      "",
      "",
      "veste",
      "",
      "",
      "cross_thin_shawl_border"
    ]
  ];
  /*********************************** */
}
