import React, { Component } from "react";
import ThreeJSComponent from "./Component3D/ThreeJSComponent";
import AccordionProduct from "./AccordionProduct";
import { Container, Row, Col } from "react-bootstrap";
import ReviewArticle from "./ReviewArticle";
import axios from 'axios';

///////////////
import "./css/style.css";
import { Button } from "semantic-ui-react";

/**
 *
 */
class DataProvider extends Component {
  /**
  * @param state = {
    * @property loading: is DataProfider queyring is still loading  
    * @property data: array storing data from data fetching, data stored is 
    the resonspe of the api-bespoke/products which returns all the products name with id
    * @property product_index:props.product_index
    * 
    * @param {*} props 
    */
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      options: null,
      responseXML: null
    };
    this.getPresta = this.getPresta.bind(this);
    this.postPresta = this.postPresta.bind(this);
  }

  async componentDidMount() {
    await this.fetchingData();
    //   this.fetchingDataOption();
  }
  async componentWillReceiveProps(nextProps) {
    if (nextProps.product_index != this.props.product_index) {
      const url =
        "api-bespoke/products/" +
        this.state.data[nextProps.product_index].id +
        "/options/";
      const responseOption = await fetch(url);
      const data_response_option = await responseOption.json();
      this.setState({ options: data_response_option.results });
    }
  }

  /**
   *
   */
  async fetchingData() {
    const response = await fetch(this.props.url);
    const data_response = await response.json();
    this.setState({ data: data_response.results });

    const url =
      "api-bespoke/products/" +
      data_response.results[this.props.product_index].id +
      "/options/";
    const responseOption = await fetch(url);
    const data_response_option = await responseOption.json();
    this.setState({ options: data_response_option.results, loading: false });
  }

  render() {
    if (this.state.loading) {
      return <div>loading...</div>;
    }
    const img = this.state.data[this.props.product_index].default_texture;
    return (
      <div>
        <ReviewArticle
          title={this.state.data[this.props.product_index].name}
          cart={this.props.cart}
          data={this.state.data}
        />
        <Container className="full-width">
          <Row>
            <Col
              lg={5}
              md={5}
              sm={12}
              xs={12}
              style={{ paddingLeft: 0, paddingRight: 0 }}
            >
              <AccordionProduct
                title={this.state.data[this.props.product_index].name}
                data={this.state.data[this.props.product_index]}
                options={this.state.options}
                props_cart={this.props.props_cart}
              />
            </Col>
            <Col md={7}>
              {/* <ThreeJSComponent product_index={this.state.data[this.props.product_index].id} default_texture={img} 
        cart={this.props.cart} cart_update={this.props.cart_update} options={this.state.options}
        title={this.state.data[this.props.product_index].name}  /> */}

              <ThreeJSComponent
                title={this.state.data[this.props.product_index].name}
                cart={this.props.cart}
                options={this.state.options}
              />
            </Col>
          </Row>

          <Button onClick={this.getPresta}>GET</Button>
          <Button onClick={this.postPresta}>POST</Button>
        </Container>
      </div>
    );
  }
}
export default DataProvider;
