import React, { Component } from "react";
import {Navbar, Nav, NavDropdown} from 'react-bootstrap'

class NavBarCustom extends React.Component{
    constructor(props) {
           super(props);
           this.state = {
                  data: null,
                  loading: true,
            };
          }
    
          async componentWillMount() {
            const response = await fetch(this.props.url);
            const data_response = await response.json();
            this.setState({ data: data_response.results, loading: false });
     }

  render() {
        
    if (this.state.loading) {
      return <div>loading...</div>;
    }
 

    if (!this.state.data) {
      return <div>Error fetching data</div>;
    }
    
    const {data} = this.state;
    var dropdown =[];
    data.map((value,index)=>{
                  current_index: 0
      dropdown.push(<NavDropdown.Item key={index} value={""+index} disabled={this.props.product_index==index}
       onClick={this.props.handler} href={"#products."+index}>{value.name}</NavDropdown.Item>)
    });
    
         return (
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand href="#home">E-Tailor</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
              <Nav.Link href="#features">Home</Nav.Link>
              <Nav.Link href="#pricing">Pricing</Nav.Link>
              
              </Nav>
                  <Nav>
                        <NavDropdown title="Modèle" id="collasible-nav-dropdown">
                            {dropdown}
                            <NavDropdown.Divider />
                                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown>

              </Nav>
          </Navbar.Collapse>
          </Navbar>
          )   
  }
};

export default NavBarCustom;
