import ScrollMenu from 'react-horizontal-scrolling-menu';
import React from 'react';
import PropTypes from "prop-types";

import './css/style.css';

// list of items
// One item component
// selected prop will be passed
const MenuItem = ({text, selected}) => {
  
  return <div 
    className={`menu-item ${selected ? 'active' : ''}`}>{text}</div>;};
      
     // All items component
     // Important! add unique key
     export const Menu = (list, selected) =>
       list.map(el => {
         const {name} = el;
      
         return <MenuItem text={name} key={name} selected={selected}
         />;
       });
      
      
       const Arrow = ({ text, className }) => {
        return <div className={className}>{text}</div>;
      };
      Arrow.propTypes = {
        text: PropTypes.string,
        className: PropTypes.string
      };
      
      export const ArrowLeft = Arrow({ text: "<", className: "arrow-prev" });
      export const ArrowRight = Arrow({ text: ">", className: "arrow-next" });
      
      
     class HorizontalNav extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
           selected:'Tissus',
           listMenu:null,
         };
         // call it again if items count changes
         this.menuItems = Menu(props.listMenu, this.state.selected);
       }

       
       render() {
         
         // Create menu from items
         if(this.state.listMenu != this.props.listMenu){
           if(this.state.listMenu == null){
              this.state.listMenu =  this.props.listMenu;
              this.menuItems = Menu(this.props.listMenu, 'Tissus');
           }else{
            this.state.listMenu =  this.props.listMenu;
            this.menuItems = Menu(this.props.listMenu, this.props.selected);
           }
         }
      
         return (
             <ScrollMenu
               data={ this.menuItems}
               arrowLeft={ArrowLeft}
               arrowRight={ArrowRight}
               selected={this.props.selected}
               onSelect={this.props.action}
               wheel={false}
               scrollToSelected={true}
               />
         );
       }
     }
     export default HorizontalNav;
