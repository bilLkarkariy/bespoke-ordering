import React, { Component } from "react";

class ReviewArticle extends React.Component {
       constructor(props) {
              super(props);
              this.state = {
                     active: false,
                     showHideArrow: "fas fa-angle-down"
              };
              this.toggleClass = this.toggleClass.bind(this);
              
       }
       toggleClass(){
              const currentState = this.state.active;
              this.setState({ active: !currentState });
              if(!currentState){
                     this.setState({showHideArrow :   "fas fa-angle-up"});
              }else{
                     this.setState({showHideArrow :   "fas fa-angle-down"});
              }

              
       }
       
       render() {
              const {cart} = this.props;
              //console.log(cart);
              var cart_detail = [];
              cart.forEach((element,index) => {
                     cart_detail.push (
                                   <li key={index}>
                                          <span className="picto" ></span>
                                          <span className="label"> {element.option_link} : </span>
                                          <span className="value">{element.name}</span>
                                   </li>);
              }); 
              return (
                     <div>
                            <div className="page-title">
                            <h1 className="big">{this.props.title} sur mesure personnalisable</h1>
                            </div>
                            <div  className={this.state.active ? 'review open': 'review'}  >
                                   <a className="head" href="#">
                                   <span className="v-align" onClick={this.toggleClass} >Descriptif de votre commande &nbsp;&nbsp;
                                   <i className={this.state.showHideArrow}> </i></span>
                                   </a>
                            <div className="content">
                     
                            <p className="product-type">{this.props.title}:</p>
                            <ul className="options">
                                   {cart_detail}
                            </ul>
                                   <div className="clear"></div>
                     
                                   <a className="close" href="#"><span className="picto"></span></a>
                                   </div>
                            </div>
                     </div>
                     );
              }
       }
       export default ReviewArticle;
       
       
       